#!/bin/sh
this=`readlink -f $0`
exec /usr/bin/env \
LD_LIBRARY_PATH=`dirname $this` \
${GDB+gdb} `dirname $this`/oop "$@"
