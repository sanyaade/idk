#!/bin/sh

# * build with debug symbols:
#   DEBUG= ./build.sh
# * build with nvg parts:
#   EnableNVG= ./build.sh
# * both:
#   DEBUG= EnableNVG= ./build.sh

cc=${CC:-g++}

this=`readlink -f $0`
thisdir=`dirname $this`
bindir="$thisdir/bin"
srcdir="$thisdir/src"
nvgdir="$thisdir/nanogui"
nvgCflags=" -I$nvgdir/include -I$nvgdir/ext/eigen \
-I$nvgdir/ext/glfw/include \
-I$nvgdir/ext/nanovg/src -DEnableNVG "
nvgLDflags=" -L$bindir -lnanogui -lGL -lGLU "

#http://www.tldp.org/LDP/abs/html/parameter-substitution.html
cflags=" -std=c++1z -w -I$srcdir \
${Profile+-pg} ${DEBUG+-g} \
-DEnablePrintAllocations -DDynamicPrint -DCppRefcounting \
${PMC+-DEnableMethodCache} ${LineNoise+-DEnableLineNoise} \
${EnableNVG+$nvgCflags}"
ldflags="${EnableNVG+$nvgLDflags}  "

mkdir -p $bindir
cmd="$cc $cflags -o $bindir/oop \
$srcdir/main.cpp \
${LineNoise+linenoise.o} $ldflags"
echo $cmd
exec $cmd

