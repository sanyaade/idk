
but you can just wrap that into a class. in fowltalk:
```smalltalk
Object subclass: #Parser variables: #(#input. #pos) classVariables: #().

def Parser char
[
    input at: position
].
def Parser consumeChar
[
    pos = pos + 1.
    self char
].


def Parser input: str pos: p
[
  input = str.
  pos = p.
  self
].


def Parser isWhitespace
[|c|
    c = self char.
    c == 9 or: [c == 10 or: [c == 32]]
].
def Parser isDigit
[
    self char between: 48 and: 57
].
def Parser isAlpha
[
    self isUpper or: [self isLower]
].
def Parser isUpper
[
    self char between: 65 and: 90
].
def Parser isLower
[
    self char between: 97 and: 122
].
def Parser isAlphaNumeric
[
    self isAlpha or: [self isDigit]
].
```

a grammar:
* `Expression = KeywordExpr`
* `KeywordExpr = BinaryExpr *(Keyword BinaryExpr)`
* `BinaryExpr = UnaryExpr BinaryOp UnaryExpr`
* `UnaryExpr = TerminalExpr *UnaryMessage`
* `TerminalExpr = ('(' Expression ')')
  | `

```smalltalk


```
