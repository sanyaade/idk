# what is this
fowltalk is mostly based on smalltalk.
everything is an object, even objects.
objects live together in an image and communicate via messages.

## getting started
dependencies:
* boost::qi
* linenoise for the repl

```sh
# clone the repo
$ git clone https://bitbucket.org/fowlmouth/idk.git
$ cd idk
# compile the VM
$ CC=gcc ./setup-linenoise.sh
$ CC=g++ LineNoise= ./build.sh
# create an image
$ ./bin/oop -i oop.img -b bootstrap
# run the repl
$ ./bin/oop -i oop.img --mmap --repl
# or run an expression
$ ./bin/oop -i oop.img --mmap -e "def Object someMethod [ stuff ]. Object someMethod"
# changes are saved to the image, someMethod will still exist
$ ./bin/oop -i oop.img --mmap -e "Object someMethod"
```
see the bootstrap file for available methods
