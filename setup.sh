#!/bin/sh
cc=${CC:-g++}

git clone --recursive --depth 1 git@github.com:wjakob/nanogui.git
cd nanogui
cmake .
make
cp libnanogui.so ../bin
cd ..

./setup_linenoise.sh
