#!/bin/sh
cc=${CC:-gcc}

git clone --recursive --depth 1 git@github.com:antirez/linenoise.git
cd linenoise
eval "$cc" -Os -c -o ../linenoise.o linenoise.c

