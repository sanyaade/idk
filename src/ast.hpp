#pragma once

#include <boost/config/warning_disable.hpp>
#define BOOST_SPIRIT_DEBUG
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/foreach.hpp>
#include <boost/variant.hpp>

namespace ast
{

    enum message_type {Keyword, Binary, Unary};
    message_type selector_type(const std::string& s)
    {
        if(std::ispunct(s[0])) return Binary;
        if(s.find(':') != std::string::npos) return Keyword;
        return Unary;
    }



    namespace tag
    {
        struct constant;
        struct variable;
        struct instvar;
        struct symbol;
        struct stringlit;
    }
    template<class Tag>
    struct ident
    {
        std::string name;
    };
    struct intlit
    {
        int value;

        intlit (const int& i): value(i) {}
        intlit (): value(0) {}
        intlit (const intlit& other): value(other.value) {}
    };


    using constant  = ident<tag::constant>;
    using variable  = ident<tag::variable>;
    using symbol    = ident<tag::symbol>;
    using stringlit = ident<tag::stringlit>;
    using instvar   = ident<tag::instvar>;

    struct nil {};
    struct truelit {};
    struct falselit {};
    struct self {};
    struct super {};
    struct thisContext {};

    struct array;
    struct message;
    struct block;
    struct assign;
    struct call_primitive;
    struct ret;
    struct dict;

    using expr = boost::make_recursive_variant<
                 nil, truelit, falselit, self, super, thisContext,
                 constant,
                 variable,
                 instvar,
                 symbol,
                 stringlit,
                 intlit,
                 array, //dict,
                 message,
                 block,
                 boost::recursive_wrapper< assign >,
                 boost::recursive_wrapper< call_primitive >,
                 boost::recursive_wrapper< ret >
                 >::type;

    struct array
    {
        std::vector<ast::expr> members;
    };
    struct message
    {
        std::string name;
        std::vector<ast::expr> args;
        message_type type() { return selector_type(name); }
    };

    struct block
    {
        std::vector<std::string> args, locals;
        std::vector<ast::expr> stmts;

        std::size_t total_locals()
        {
            return args.size() + locals.size();
        }
    };
    struct assign
    {
        std::string dest;
        ast::expr value;
    };
    struct ret
    {
        ast::expr value;
    };
    struct call_primitive
    {
        INT_TYPE id;
        std::vector<ast::expr> args;
    };
    struct dict: array
    {
    };


    struct method_def: block
    {
        std::string class_name, selector;
    };

    using toplevel_stmt = boost::variant<method_def, expr>;
}


#include <boost/fusion/adapted.hpp>

BOOST_FUSION_ADAPT_STRUCT(ast::constant,  (std::string, name))
BOOST_FUSION_ADAPT_STRUCT(ast::variable,  (std::string, name))
BOOST_FUSION_ADAPT_STRUCT(ast::instvar, (std::string, name))
BOOST_FUSION_ADAPT_STRUCT(ast::symbol,    (std::string, name))
BOOST_FUSION_ADAPT_STRUCT(ast::stringlit, (std::string, name))
BOOST_FUSION_ADAPT_STRUCT(ast::intlit,    (int, value))
BOOST_FUSION_ADAPT_STRUCT(ast::array,     (std::vector<ast::expr>, members))
BOOST_FUSION_ADAPT_STRUCT(ast::dict,      (std::vector<ast::expr>, members))
BOOST_FUSION_ADAPT_STRUCT(ast::message,   (std::string, name)(std::vector<ast::expr>, args))
BOOST_FUSION_ADAPT_STRUCT(ast::block,
                          (std::vector<std::string>, args)
                          (std::vector<std::string>, locals)
                          (std::vector<ast::expr>, stmts))
BOOST_FUSION_ADAPT_STRUCT(ast::assign,
                          (std::string, dest)
                          (ast::expr, value))
BOOST_FUSION_ADAPT_STRUCT(ast::call_primitive,
                          (INT_TYPE, id)
                          (std::vector<ast::expr>, args))
BOOST_FUSION_ADAPT_STRUCT(ast::method_def,
                          (std::vector<std::string>, args)
                          (std::vector<std::string>, locals)
                          (std::vector<ast::expr>, stmts)
                          (std::string, class_name)
                          (std::string, selector))
BOOST_FUSION_ADAPT_STRUCT(ast::ret, (ast::expr, value))

//BOOST_FUSION_ADAPT_STRUCT(ast::truelit, ())
//BOOST_FUSION_ADAPT_STRUCT(ast::falselit, ())
//BOOST_FUSION_ADAPT_STRUCT(ast::nil, ())



class code_repr: public boost::static_visitor<std::string>
{
public:
    int d, i;

    code_repr (int depth = 0, int indent = 1): d(depth), i(indent)
    {}


    std::string operator()(ast::nil&) const
    {
        return "nil";
    }
    std::string operator()(ast::truelit&) const
    {
        return "true";
    }
    std::string operator()(ast::falselit&) const
    {
        return "false";
    }
    std::string operator()(ast::self&) const
    {
        return "self";
    }
    std::string operator()(ast::super&) const
    {
        return "super";
    }
    std::string operator()(ast::thisContext&) const
    {
        return "thisContext";
    }

    std::string operator()(ast::intlit& i) const
    {
        return std::to_string(i.value);
    }
    std::string operator()(ast::variable& v) const
    {
        return v.name;
    }
    std::string operator()(ast::instvar& v) const
    {
        return "@"+v.name;
    }
    std::string operator()(ast::constant& c) const
    {
        return c.name;
    }
    std::string operator()(ast::stringlit& s) const
    {
        return "'" + s.name + '\'';
    }
    std::string operator()(ast::symbol& s) const
    {
        return s.name;
    }

    std::string indent(int indent) const
    {
        if(indent < 0) indent = 0;
        std::ostringstream s;
        for(; indent; --indent)
            s << "  ";
        return s.str();
    }
    std::string indent() const
    {
        return indent(i);
    }
    std::string operator()(ast::block& b) const
    {
        std::ostringstream s;
        s << "[";
        if(!b.args.empty() || !b.locals.empty())
        {
            s << "|" ;
            for(const auto& arg: b.args) s << " :" << arg;
            for(const auto& loc: b.locals) s << ' ' << loc;
            s << " | ";
        }
        s << '\n';
        for(int ii = 0; ii < b.stmts.size(); ++ii)
        {
            s << indent() << code_repr(0, i+1)(b.stmts.at(ii));
            if(ii < (b.stmts.size()-1)) s << ".\n";
        }
        s << '\n' << indent(i-1) << ']';
        return s.str();
    }

    std::string operator()(ast::message& m) const
    {
        std::ostringstream s;

        auto do_parens = [&](int i){
            if(d > i) return true;
            return false;
        };
#define PARENS(p,stmts) \
        {                    \
            int new_d = d+1; \
            if(do_parens(p)) \
            {                \
                new_d = 0;   \
                s << '(';    \
            }                \
            stmts                      \
            if(do_parens(p)) s << ')'; \
        }

        switch(m.type())
        {
        case ast::Keyword:
        {
            std::size_t last_p = 0;
            std::size_t p = m.name.find(':');
            int idx = 1;

            PARENS(0,{
                s << code_repr(new_d, i)(m.args[0]);
                while(p != std::string::npos)
                {
                    s << ' ' << m.name.substr(last_p, p+1)
                      << ' ' << code_repr(new_d, i)(m.args[idx++]);
                    last_p = p+1;
                    p = m.name.find(':', last_p);
                }
            })
            break;
        }
        case ast::Binary:
            PARENS(1,{
                s << code_repr(new_d, i)(m.args[0]) 
                  << ' ' << m.name 
                  << ' ' << code_repr(new_d, i)(m.args[1]);
            })
            break;
        case ast::Unary:
            PARENS(2,{
                s << code_repr(new_d, i)(m.args[0])
                  << ' ' << m.name;
            })
            break;
        }
        return s.str();

#undef PARENS

        // s << "(" << m.name;
        // for(auto& it: m.args)
        // {
        //     s << ' ' << boost::apply_visitor(code_repr(d+1, i), it);
        // }
        // s << ')';
        // return s.str();
    }
    std::string operator()(ast::array& a) const
    {
        std::ostringstream s;
        s << "#(" ;
        for(auto& it : a.members)
            s << boost::apply_visitor(code_repr(d+1, i), it) << " ";
        s << ")";
        return s.str();
    }

    std::string operator()(ast::assign& a) const
    {
        return a.dest + " := " + boost::apply_visitor(code_repr(0, i), a.value);
    }
    std::string operator()(ast::call_primitive& c) const
    {
        std::ostringstream s;
        s << "<" << c.id;
        for(auto& arg: c.args)
            s << ' ' << code_repr(5, i+1)(arg);
        s << ">" ;
        return s.str();
    }

    std::string operator()(ast::method_def& m) const
    {
        std::ostringstream s;
        s << "def " << m.class_name;
        auto type = ast::selector_type(m.selector);
        switch(type)
        {
        case ast::Keyword:
        {
            std::size_t last_p = 0;
            std::size_t p = m.selector.find(':');
            auto iter = m.args.begin();

            while(p != std::string::npos)
            {
                s << ' ' << m.selector.substr(last_p, p+1)
                  << ' ' << *iter++;
                last_p = p+1;
                p = m.selector.find(':', last_p);
            }
        }   break;
        case ast::Binary:
            s << ' ' << m.selector << ' ' << m.args[0];
            break;
        case ast::Unary:
            s << ' ' << m.selector;
            break;
        }
        s << '\n' << (*this)(dynamic_cast<ast::block&>(m));
        return s.str();

        // return "def " + m.class_name +
        //        " #" + m.selector +
        //        (*this)(dynamic_cast<ast::block&>(m));

    }
    std::string operator()(ast::expr& expr) const
    {
        return boost::apply_visitor(*this, expr);
    }

    std::string operator()(ast::ret& r) const
    {
        return "^ " + (*this)(r.value);
    }

};


class ast_print: public boost::static_visitor<>
{
public:

    void operator()(ast::nil&) const
    {
        std::cout << "nil";
    }
    void operator()(ast::truelit&) const
    {
        std::cout << "true";
    }
    void operator()(ast::falselit&) const
    {
        std::cout << "false";
    }
    void operator()(ast::self&) const
    {
        std::cout << "self";
    }
    void operator()(ast::super&) const
    {
        std::cout << "super";
    }
    void operator()(ast::thisContext&) const
    {
        std::cout << "thisContext";
    }

    void operator()(ast::intlit& i) const
    {
        std::cout << i.value;
    }
    void operator()(ast::variable& v) const
    {
        std::cout << v.name;
    }
    void operator()(ast::instvar& v) const
    {
        std::cout << '@' << v.name;
    }
    void operator()(ast::constant& c) const
    {
        std::cout << "!" << c.name;
    }
    void operator()(ast::stringlit& s) const
    {
        std::cout << '\'' << s.name << '\'';
    }
    void operator()(ast::symbol& s) const
    {
        std::cout << '#' << s.name;
    }
    void operator()(ast::block& b) const
    {
        std::cout << "[";
        if(!b.args.empty() || !b.locals.empty())
        {
            std::cout << "|" ;
            for(const auto& arg: b.args) std::cout << " :" << arg;
            for(const auto& loc: b.locals) std::cout << ' ' << loc;
            std::cout << " | ";
        }
        for(auto& it: b.stmts)
        {
            boost::apply_visitor(ast_print(), it);
            std::cout << ". ";
        }
        std::cout << "]";
    }
    void operator()(ast::message& m) const
    {
        std::cout << "(" << m.name;
        for(auto& it: m.args)
        {
            std::cout << ' ';
            boost::apply_visitor(ast_print(), it);
        }
        std::cout << ')';

    }
    void operator()(ast::array& a) const
    {
        std::cout << "#(" ;
        for(auto& it : a.members)
        {
            boost::apply_visitor(ast_print(), it);
            std::cout << " ";
        }
        std::cout << ")";
    }

    void operator()(ast::assign& a) const
    {
        std::cout << a.dest << " := ";
        boost::apply_visitor(ast_print(), a.value);
    }
    void operator()(ast::call_primitive& c) const
    {
        std::cout << "<" << c.id;
        for(auto& arg: c.args)
        {
            std::cout << ' ';
            boost::apply_visitor(ast_print(), arg);
        }
        std::cout << ">" ;

    }

    void operator()(ast::method_def& m) const
    {
        std::cout << "def " << m.class_name
                  << " #" << m.selector << std::endl;
        this->operator()(dynamic_cast<ast::block&>(m));

    }
    void operator()(ast::expr& expr) const
    {
        boost::apply_visitor(*this, expr);
    }

    void operator()(ast::ret& r) const
    {
        std::cout << "^ ";
        boost::apply_visitor(*this, r.value);
    }

};







