#pragma once
#include <typeinfo>
#include <cxxabi.h>

template<typename T>
std::string demangled_name()
{
    int status;
    const char* name = typeid(T).name();
    char* res = abi::__cxa_demangle(name, nullptr, nullptr, &status);
    const char* demangled = (status == 0) ? res : name;
    std::string retval(demangled);
    std::free(res);
    return retval;
}
