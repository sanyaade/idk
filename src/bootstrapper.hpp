

#pragma once

#include "codegen.hpp"
#include "interp.hpp"
#include <boost/spirit/include/support_istream_iterator.hpp>

namespace interp {

using std::cout;
using std::endl;

class bootstrapper
{
public:
    oop_space* sp;
    VM vm;


    bootstrapper (oop_space* sp):
        sp(sp), globals(), symbols(),
        vm(sp)
    {
    }

    using map_t = std::map<std::string, oop_handle>;
    map_t globals;
    map_t symbols;
    // oop_handle //symclass,
      // classclass, objclass;




    const oop_handle& symbols_array()
    {
        return symclass().get_obj()->get_field(oop_class_slots::slots + 1);
    }
    oop_handle new_symbol(const std::string& name)
    {
        int idx;
        if( symbols_array().is_nil())
        {
            map_t::const_iterator it = symbols.find(name);
            if(it != symbols.end())
            {
                return it->second;
            }

        }
        else
        {
            idx = ordered_search(symbols_array(), name.c_str());
            const auto& s = symbols_array().get_obj()->get_field(idx);
            if(cmp_symbol( s.get_obj()->cstr(), name.c_str() ) == 0)
                return s;
        }

        oop_handle result = new_string(name, symclass());
        if(symbols_array().is_nil())
            symbols.insert(std::make_pair(name, result));
        else
            symclass().get_obj()->set_field(
                oop_class_slots::slots+1,
                array_insert(symbols_array(), idx, result));

        return result;

        // obj_ptr o = alloc_bits(name.size()+1);
        // assert(copy_str(name, o));
        // assert(o->bytes[name.size()] == '\0');
        // //o->bytes[name.size()] = '\0';
        // //set class to symbolclass
        // o->set_class(symclass);
        // return oop_handle(o);
        // //return oop_handle::nil_obj;
    }


    oop_handle new_class (const std::string& name,
                       const oop_handle& superclass,
                       std::size_t classVars = 0)
    {

        //assert(globals.find(name) == globals.end());
        assert(find_global(name).is_nil());

        auto sym = new_symbol(name);
        obj_ptr res = alloc_slots(oop_class_slots::slots + classVars);
        assert(res);

        //res->set_class(classclass);
        res->set_field(oop_class_slots::name, sym);

        add_global(name, oop_handle(res));

        res->set_field(oop_class_slots::superclass, superclass);

        return oop_handle(res);
    }
    oop_handle new_class (const std::string& name)
    {
        return new_class(name, oop_handle::nil_obj, 0);
    }


    void t1()
    {
#define out(s) (std::cout.width(13), std::cout << std::left << (s))
        std::vector<std::string> gs({"Object", "MetaObject", "Class", "MetaClass"});
        out(""),
            out("class"),
            out("(class)"),
            out("(superclass)"),
            out("(refcount)") << std::endl;

        for(auto it = gs.begin(); it != gs.end(); it++)
        {
            const oop_handle& obj = globals.at(*it);
            const oop_handle& cls = obj.get_class();
            const oop_handle& supercls = obj.get<TAG_OBJ>()->get_field(oop_class_slots::superclass);

            out(*it) ;
            out(obj.repr()) ;
            out(cls.repr()) ;
            out(supercls.repr()) ;
            out(obj.get<TAG_OBJ>()->get_refcount()) ;
            out(obj.get_obj()->get_field(oop_class_slots::variables).repr()) ;

        }

        oop_handle o1;
        std::cout <<
                  o1.repr() << std::endl;
#undef out
    }

    void test_classes()
    {
        const auto& sz = oop_class_slots::slots * sizeof(oop_handle);
#define test(cls) do{\
    const auto& o = find_global(cls); \
    assert(o.get_obj()->get_size() >= sz); \
    std::cout << cls << ": " << \
     "inst size "<< o.get_obj()->get_field(oop_class_slots::instance_size).repr() << "/" << \
     "class inst size"<< o.get_class().get_obj()->get_field(oop_class_slots::instance_size).repr() << "/" << \
     "my size " << o.get_obj()->get_size() << std::endl; \
  } while(0)
        test("Object");
        test("Class");
        test("MetaObject");
        test("MetaClass");
        test("Symbol");
        test("MetaSymbol");
        test("Array");

        const auto& o = find_global("MetaSymbol");
        const auto& arr = o.get_obj()->get_field(oop_class_slots::variables);
        assert(! arr.is_nil());
        cout << arr.get_obj()->get_size() << endl;

        const auto& meta_array = find_global("Array").get_obj()->get_class();
        cout << & meta_array.get_obj()->get_field(oop_class_slots::name).get_obj()->bytes[0] << endl;


#undef test
    }

    const oop_handle& symclass()
    {
        return sp->this_header()->get<image::sym_class>();
    }
    void strapboots(const std::string& file)
    {
        //magic here

        obj_ptr image = alloc_slots(image::slots);
        sp->this_header()->image = oop_handle(image);

        //create Symbol
        auto sc = alloc_slots(oop_class_slots::slots + 1);
        //symclass = oop_handle(sc);
        sp->this_header()->set<image::sym_class>( oop_handle(sc) );

        sc->set_field(oop_class_slots::name, new_symbol("Symbol"));
        add_global("Symbol", symclass());

        //assert(symclass.get_offset() != 0);
        std::cout << symclass().get<TAG_OBJ>().get_offset() << std::endl;

        //create object, metaobject
        auto objclass = oop_handle(new_class("Object", oop_handle::nil_obj));
        auto metaobj = oop_handle(new_class("MetaObject", oop_handle::nil_obj));
        //objclass->set_field(oop_class_slots::superclass, );

        auto arrclass = new_class("Array", objclass);
        auto arr_o = oop_handle(arrclass);
#   define fixcls(p,cls) \
    { const auto& ivs = p->get_field(oop_class_slots::variables); \
      if(ivs.is_obj() && ivs.get_class().is_nil())               \
        { ivs.get_obj()->set_field(0, arr_o); cout << "fixed " #p << endl;} \
      if(p->get_class().is_nil()) p->set_field(0, cls);           \
    } while(0)

        //if(p->get_field(oop_class_slots::variables).is_obj()) \
        //   p->get_field(oop_class_slots::variables).get_obj()->set_field(0, cls);  \

        //fixcls(arrclass, objclass);

        //create class, metaclass
        oop_handle classclass = new_class("Class", objclass);
        oop_handle metaclass = new_class("MetaClass", metaobj);
        //classclass->set_field(oop_class_slots::superclass, objclass);

        //fix a few classes
        //symclass->get_class() =
        fixcls(objclass.get_obj(), metaobj);
        fixcls(classclass.get_obj(), metaclass);

        //objclass.get_obj()->set_class(metaobj);
        //classclass.get_obj()->set_class(metaclass);
        metaclass.get_obj()->set_class(classclass);
        metaobj.get_obj()->set_class(classclass);


/*
 class  | class of class | superclass
Object     | MetaObject  | nil
MetaObject | Class       | Class
Class      | MetaClass   | Object
MetaClass  | Class       | MetaObject

*/

        //set super fields
#   define setisize(cls,i) cls.get_obj()->set_field(oop_class_slots::instance_size, oop_handle(i))
        setisize(objclass, 0);
        setisize(classclass, oop_class_slots::slots);
        setisize(metaclass, oop_class_slots::slots);
        setisize(metaobj, oop_class_slots::slots);
#   define setsuper(cls1,cls2) \
    do{\
    cls1->set_field(oop_class_slots::superclass, cls2); \
    }while(0)
    // if(cls1->get_field(oop_class_slots::instance_size).is_nil())\
    //     cls1->set_field(oop_class_slots::instance_size,\
    //      cls2.get_obj()->get_field(oop_class_slots::instance_size));\

        setsuper(classclass.get_obj(), objclass);
        setsuper(metaobj.get_obj(), classclass);
        setsuper(metaclass.get_obj(), metaobj);
        setsuper(sc, objclass);
#   undef setsuper
#   undef setisize

        auto metasym = new_class("MetaSymbol", metaobj);
        metasym.get_obj()->set_class(classclass);
        sc->set_class(metasym);


        //create nil
        auto nilclass = new_class("NilClass", objclass);

        nilclass.get_obj()->set_field(0//oop_class_slots::superclass
            , classclass);
        sp->this_header()->set<image::nil_class>( nilclass );

        //auto dict_class = new_class("Dictionary", objclass);
        //objclass.get_obj()->set_field(oop_class_slots::methods, new_dict());

        objclass.get_obj()->set_field(oop_class_slots::instance_size,
            oop_handle(0));
        classclass.get_obj()->set_field(oop_class_slots::instance_size,
            oop_handle(oop_class_slots::slots));


        std::ifstream f(file);
        f.unsetf(std::ios::skipws);
        std::string code;
        f.seekg(0, std::ios::end);
        code.reserve(f.tellg());
        f.seekg(0, std::ios::beg);
        code.assign(
            std::istreambuf_iterator<char>(f),
            std::istreambuf_iterator<char>());
        run_code(code.begin(), code.end());

        create_globals_dict();
        fix_globals();
        create_symbol_symbols();

        //build image
        //image->set_field(image::globals, global_dict());
        image->set_field(image::int_class, find_global("Integer"));
        //image->set_field(image::nil_class, find_global("NilClass"));
        image->set_field(image::true_class, find_global("True"));
        image->set_field(image::false_class, find_global("False"));
        
    }


    void fix_globals()
    {
        const oop_handle& symclass = find_global("Symbol");
        const oop_handle& gd = global_dict();
        const auto& keys = gd.get_obj()->get_field(1);
        for(int i = 1; i < keys.get_obj()->num_slots(); ++i)
        {
            const auto& key = keys.get_obj()->get_field(i);
            const auto& val = gd.get_obj()->get_field(2).get_obj()->get_field(i);
            if(key.get_class() != symclass)
            {
                PrintWarn(key.get_obj()->cstr() << " is not a symbol! " << key.get_class().repr() );
                key.get_obj()->set_field(0, symclass);
            }
            
            if(val.get_obj()->num_slots() >= oop_class_slots::slots)
            {
                const auto& name = val.get_obj()->get_field(oop_class_slots::name);
                if(name.is_nil())
                {
                    PrintWarn("class " << key.get_obj()->cstr() << " had no name.");
                    val.get_obj()->set_field(oop_class_slots::name, key);
                }
                else if(name != key)
                {
                    PrintWarn("class " << key.get_obj()->cstr() << " inconsistent name: " << name.repr());
                    val.get_obj()->set_field(oop_class_slots::name, key);
                }
                if(val.get_obj()->get_field(oop_class_slots::methods).is_nil())
                {
                    PrintWarn("class " << key.repr() << " methods is nil!");
                    val.get_obj()->set_field(oop_class_slots::methods, new_dict());
                }
                const auto& meths = val.get_obj()->get_field(oop_class_slots::methods);
                if(meths.get_class().is_nil())
                    meths.get_obj()->set_field(0, find_global("Dictionary"));
                if(meths.get_obj()->get_field(1).get_class().is_nil())
                    meths.get_obj()->get_field(1).get_obj()->set_field(0, find_global("OrderedArray"));
                if(meths.get_obj()->get_field(2).get_class().is_nil())
                    meths.get_obj()->get_field(2).get_obj()->set_field(0, find_global("Array"));

            }
        }
        // for(int i = 1; i <= keys.get_obj()->num_slots(); ++i)
        //     Print(i << ": " << keys.get_obj()->get_field(i).get_obj()->cstr());
    }


    const oop_handle& global_dict()
    {
        return sp->globals();
    }

    oop_handle find_global (const std::string& name)
    {
        if(global_dict().is_nil())
        {
            auto it = globals.find(name);
            if(it == globals.end())
                return oop_handle::nil_obj;
            else
                return it->second;
        }
        // Print("** using global_dict()");
        return dict_search(global_dict(), name.c_str());
    }

    bool add_global (const std::string& name, oop_handle val)
    {
        std::cout << "adding global " << name << ": " << val.repr() << std::endl;
        if(global_dict().is_nil())
        {
            return globals.emplace(name, val).second;
        }
        oop_handle sym = new_symbol(name);
        return dict_insert(global_dict(), sym, val);
    }

    void create_globals_dict()
    {
        if(global_dict().is_nil())
        {
            sp->this_header()->image.get_obj()->set_field(image::globals, new_dict());
            add_global("Globals", global_dict());
            //global_dict() = new_dict();
            for(const auto& g: globals)
                dict_insert(global_dict(), new_symbol(g.first), g.second);
        }
    }
    void create_symbol_symbols()
    {
        if(symbols_array().is_nil())
        {
            oop_handle kls = find_global("OrderedArray");
            if(kls.is_nil())
            {
                PrintError("OrderedArray is nil??");
                return;
            }
            oop_handle arr = new_array(0, find_global("OrderedArray"));
            for(const auto& s: symbols)
                arr = array_insert(arr, ordered_search(arr, s.first.c_str()), s.second);
            symclass().get_obj()->set_field(oop_class_slots::slots+1, arr);
        }
    }

    int show_globals()
    {

        if(sp->globals().is_nil())
        {Print("globals is nil"); return 1; }

        const auto& keys = sp->globals().get_obj()->get_field(1);
        if(keys.is_nil())
        {Print("keys is nil"); return 1; }

        auto ns = keys.get_obj()->num_slots();
        for(int i = 1; i <= ns; ++i)
        {
            Print(i << "/" << ns);
            Print(keys.get_obj()->get_field(i).get_obj()->cstr());
        }
    }


    oop_handle new_array(std::size_t slots, const oop_handle& klass)
    {
        obj_ptr res = alloc_slots(slots, klass);
        for(std::size_t i = 1; i <= slots; i++)
        {
            res->set_field(i, oop_handle::nil_obj);
        }
        return oop_handle(res);
    }
    oop_handle new_array(std::size_t slots)
    {
        return new_array(slots, find_global("Array"));
    }


    oop_handle new_dict()
    {
        obj_ptr res = alloc_slots(2);
        res->set_class(find_global("Dictionary"));
        res->set_field(1, new_array(0, find_global("OrderedArray")));
        res->set_field(2, new_array(0, find_global("Array")));
        return oop_handle(res);
    }
    /*
    oop_handle handle_send (oop_handle ctx, node* n)
    {
      auto c = dynamic_cast<container*>(n);
      auto msg = dynamic_cast<ident*>(c->at(0));
      auto self = c->at(1);
      std::cout << " send: " << msg->get_value() << std::endl;
      return oop_handle::mk<TAG_NIL>();
    }
    */
    auto find_or_make_class (const std::string& name)
    {
        oop_handle val;
        oop_handle klass = find_global(name);
        if(klass.is_nil())
        // auto it = globals.find(name);
        // if(it == globals.end())
        {
            val = oop_handle(new_class(name));
            add_global(name, val);
            return val;
        }
        else
            return klass;//val = it->second;
        //auto val = globals.at(id->get_value());
        return val;
    }

    void runSexp (const std::string &input)
    {
        /*
        oop_handle ctx;
        input_state state;
        state.pos = 0;
        state.input = input;
        node *res = parse_expr(state);
        while(res)
        {
          eval(ctx, res);
          delete res;
          res = parse_expr(state);
        }
        */
    }



    // oop_handle new_context (const oop_handle& recv, const oop_handle& caller, const oop_handle& method)
    // {
    //     // auto p = alloc_slots(context::slots, find_global("Context"));
    //     // p->set_field(context::method, method);
    //     // p->set_field(context::stack, new_array(16));
    //     // p->set_field(context::ip, oop_handle(0));
    //     // p->set_field(context::sp, oop_handle(0));
    //     // return oop_handle(p);
    //     return create_method_context(recv, method, caller);
    // }

    // void define_method (ast::method_def& mdef)
    // {
    //     oop_handle klass = find_global(mdef.class_name);

    //     if(klass.is_nil())
    //     {
    //         PrintError("Class not found: " << mdef.class_name);
    //         return;
    //     }

    //     oop_handle mclass = find_global("Method");
    //     if(mclass.is_nil())
    //     {
    //         PrintError("Method class does not exist yet!");
    //         return;
    //     }

    //     auto sym = new_symbol(mdef.selector);
    //     auto meth = new_method(sym);

    //     // obj_ptr new_method_p = alloc_slots(oop_method_slots::_num_slots, mclass);
    //     // if(new_method_p.is_null())
    //     // {
    //     //     PrintError("failed to allocate Method " << mdef.selector);
    //     //     return;
    //     // }
    //     // new_method_p->set_field(oop_method_slots::name, sym);

    //     // oop_handle meth = oop_handle(new_method_p);

    //     auto ctx = new vm::block_builder(meth, klass);
    //     ctx->get_locals().insert(ctx->get_locals().end(), mdef.args.begin(), mdef.args.end());
    //     ctx->get_locals().insert(ctx->get_locals().end(), mdef.locals.begin(), mdef.locals.end());
    //     auto visitor = vm::codegen(ctx);
    //     visitor.visit_statements(mdef.stmts);
    //     // auto it = mdef.stmts.begin();
    //     // auto last = mdef.stmts.end();
    //     // while(it < last)
    //     // {
    //     //     boost::apply_visitor(visitor, *it);
    //     //     it++;
    //     //     if(it != last) ctx->pop();
    //     //     //if it != last { pop }
    //     // }
    //     ctx->method_end();

    //     Print("defining " <<
    //         mdef.selector << " on " << mdef.class_name << ": " <<
    //         klass.repr()
    //     );

    //     oop_handle bytecodes = new_bytearray(
    //         ctx->get_bytes().begin(), ctx->get_bytes().end(),// + ctx->get_bytes().size(), //end(),
    //         find_global("ByteArray"));
    //     meth.get_obj()->set_field(oop_method_slots::bytecodes, bytecodes);

    //     oop_handle methods = klass.get_obj()->get_field(oop_class_slots::methods);
    //     if(methods.is_nil())
    //     {
    //         std::cout << "warning: creating methods on " << mdef.class_name << std::endl;
    //         methods = new_dict();
    //         klass.get_obj()->set_field(oop_class_slots::methods, methods);
    //     }
    //     else if(methods.get_obj()->get_class().is_nil())
    //     {
    //         std::cout << "warning: methods class was nil " << mdef.class_name << std::endl;
    //         methods.get_obj()->set_field(0, find_global("Dictionary"));
    //     }

    //     meth.get_obj()->set_field(oop_method_slots::locals, oop_handle(mdef.args.size() + mdef.locals.size()));
    //     meth.get_obj()->set_field(oop_method_slots::code,
    //         new_string(code_repr()(mdef), find_global("String")));
    //     meth.get_obj()->set_field(oop_method_slots::klass, klass);

    //     // PrintCode( bytecodes.get_obj().get_offset() );
    //     // PrintCode( bytecodes.get_obj()->get_refcount() );

    //     // PrintCode( meth.get_obj()->get_refcount() );

    //     dict_insert(
    //         methods,//mclass.get_obj()->get_field(oop_class_slots::methods),
    //         meth.get_obj()->get_field(oop_method_slots::name),
    //         meth);

    //     // PrintCode( meth.get_obj()->get_refcount() );

    // }

    struct printer
    {
        typedef boost::spirit::utf8_string string;

        void element(string const& tag, string const& value, int depth) const
        {
            for (int i = 0; i < (depth*4); ++i) // indent to depth
                std::cout << ' ';

            std::cout << "tag: " << tag;
            if (value != "")
                std::cout << ", value: " << value;
            std::cout << std::endl;
        }
    };

    void print_info(boost::spirit::info const& what)
    {
        using boost::spirit::basic_info_walker;

        printer pr;
        basic_info_walker<printer> walker(pr, what.tag, 0);
        boost::apply_visitor(walker, what.value);
    }

    oop_handle mk_symbol_array(const std::vector<ast::expr>& node, const oop_handle& klass)
    {
        int slots = node.size();
        obj_ptr res = alloc_slots(slots, klass);
        for(int i = 1; i <= node.size(); ++i)
        {
            const ast::symbol* s = boost::get<ast::symbol>(&node[i-1]);
            if(!s)
            {
                std::cout << "error: not a symbol: ";
                boost::apply_visitor(ast_print(), const_cast<ast::expr&>(node[i-1]));
            }
            else
                res->set_field(i, new_symbol(s->name));
        }
        return oop_handle(res);
    }
    oop_handle mk_symbol_array(const std::vector<ast::expr>& node)
    {
        return mk_symbol_array(node, find_global("Array"));
    }

    //evaluator
    /*class ast_eval: public boost::static_visitor<>
    {
        bootstrapper& b;
    public:
        ast_eval(bootstrapper& b): b(b)
        {
        }

        void operator() ()
    }*/

    class visitor: public boost::static_visitor<>
    {
        bootstrapper& b;
    public:

        visitor(bootstrapper& bootst): b(bootst)
        {
        }

        void error (const std::string &msg) const
        {
            std::cout << "[Error] " << msg << std::endl;
        }

        void operator() (ast::expr& x) const
        {
            Print(code_repr()(x));
            //boost::apply_visitor(code_repr(), x);
            ast::message* m = boost::get<ast::message>(&x);
            if(m)
            {
                if(m->name == "subclass:variables:classVariables:")
                {
                    std::string
                        &class_name = boost::get<ast::symbol>(m->args[1]).name,
                        &super_class_name = boost::get<ast::symbol>(m->args[0]).name,
                        meta_name = "Meta" + class_name;
                    ast::array
                        &classVars = boost::get<ast::array>(m->args[3]),
                        &instvars = boost::get<ast::array>(m->args[2]) ;
                    std::size_t nClassVars = classVars.members.size();
                    oop_handle klass = b.find_global(class_name), super_class = b.find_global(super_class_name);
                    oop_handle meta = b.find_global(meta_name);
                    if(super_class.is_nil())
                    {
                        error("class not found "+super_class_name);
                        return;
                    }
                    if(meta.is_nil())
                    {
                        meta = oop_handle(b.new_class(
                            meta_name,
                            super_class.get_class(), //self.class
                            0));
                    }
                    else
                    {
                        meta.get_obj()->set_field(
                            oop_class_slots::superclass,
                            b.find_global("Meta"+super_class_name));
                    }
                    meta.get_obj()->set_field(0, b.find_global("Class"));
                    if(klass.is_nil())
                    {
                        klass = oop_handle(b.new_class(
                            class_name,
                            super_class,
                            nClassVars
                        ));
                    }
                    else
                    {
                        klass.get_obj()->set_field(oop_class_slots::superclass, super_class);
                    }
                    klass.get_obj()->set_field(0, meta);

                    Print(code_repr()(x)); //boost::apply_visitor(code_repr(), x);
                    std::cout << std::endl << "--";
                    SHOWSRC(super_class_name);
                    //setup instance size and inst methods lists
                    auto fix_cls = [&](const oop_handle& cls, const oop_handle& super, const std::vector<ast::expr>* instvars)
                    {
                        const auto& ivs = cls.get_obj()->get_field(oop_class_slots::variables);
                        if(ivs.is_obj() && ivs.get_class().is_nil())
                            ivs.get_obj()->set_field(0, b.find_global("Array"));

                        oop_handle super_sz = super.get_obj()->get_field(oop_class_slots::instance_size);
                        SHOWSRC(super_sz.repr());
                        cls.get_obj()->set_field(
                            oop_class_slots::instance_size,
                            oop_handle( super_sz.get_int() + (instvars ? instvars->size() : 0)));
                        cls.get_obj()->set_field(
                            oop_class_slots::variables,
                            (instvars
                             ? ((instvars->size() == 0)
                                 ? oop_handle::nil_obj
                                 : b.mk_symbol_array(*instvars))
                             : oop_handle::nil_obj)
                        );
                        const oop_handle& methods = cls.get_obj()->get_field(oop_class_slots::methods);
                        if(methods.is_nil())
                            cls.get_obj()->set_field(oop_class_slots::methods, b.new_dict());
                    };
                    fix_cls(klass, super_class, &instvars.members);
                    const auto& meta_super = b.find_global("Meta"+super_class_name);
                    fix_cls(meta, meta_super, &classVars.members);

                    {
                        const oop_handle& meta_super2 = meta.get_obj()->get_field(oop_class_slots::superclass);
                        SHOWSRC(meta_super2.repr());

                    }

                    PrintCode( m->args.size() );
                    return;
                }

                //make a codegen context and run the expression in a VM
                b.vm.process = compile_anonymous_method_process(x);
                //execute
                b.vm.run();


            }

            // boost::apply_visitor(ast_print(), x);



            


        }
        void operator() (ast::method_def& m) const
        {
            //boost::apply_visitor(
            // Print(code_repr()(m));
            // b.define_method(m);
            Print("error: method defining is disabled during bootstrapping");
        }
    };

    // oop_handle create_context (oop_handle method)
    // {
    //     oop_handle c = oop_handle(alloc_slots(context::slots));
    //     {
    //         auto p = c.get_obj();
    //         p->set_field(context::ip, oop_handle(0));
    //         p->set_field(context::sp, oop_handle(0));


    //     }

    // }

    // oop_handle method_call_context (oop_handle caller, oop_handle recv, std::string msg, std::vector<oop_handle> args)
    // {
    //     oop_handle message = lookup_method(recv, msg.c_str());
    //     if(message.is_nil()) return message;

    //     oop_handle res = new_context(recv, caller, message);

    //     return res;
    // }

    // oop_handle send (oop_handle recv, std::string msg, std::vector<oop_handle> args)
    // {
    //     auto c = method_call_context(oop_handle::nil_obj, recv, msg, args);

    //     VM vm(sp);
    //     vm.run();

    // }

    template<typename Iter>
    using mdef = method_def<Iter, boost::spirit::ascii::space_type>;
    // template<typename Iter>
    using toplevel_stmts = std::vector<ast::toplevel_stmt >;

    template<typename Iter>
    bool parse_code (Iter start, Iter end,
        toplevel_stmts& out)
    {
        using Skip = boost::spirit::ascii::space_type;
        // using method_def = method_def<Iter, Skip>;
        // using Result = typename method_def::toplevel_stmt;

        mdef<Iter> gram;
        qi::rule<Iter, toplevel_stmts(), Skip> grammar =
          (gram.stmt % qi::lit('.')) >> -qi::lit('.');

        out.resize(0);
        try{
            if(!boost::spirit::qi::phrase_parse(start, end, grammar, boost::spirit::ascii::space, out))
                return false;
        }
        catch(const boost::spirit::qi::expectation_failure<std::string::const_iterator>& e)
        {
            // Print("incomplete expression")
            std::cout << "expected: ";
            print_info(e.what_);
            std::cout << "got: '"
            << std::string(e.first, e.last) << '\'' << std::endl;
            return false;
        }
        return true;
    }

    void run_code (toplevel_stmts& stmts)
    {
        for(auto& it: stmts)
        {
            boost::apply_visitor(visitor(*this), it);
            std::cout << std::endl;
        }
    }

    template<typename Iter>
    void run_code (Iter start, Iter end)
    {
        toplevel_stmts stmts;
        if(parse_code(start, end, stmts))
            //run_code<Iter>(stmts);
            run_code(stmts);
    }

    void run_code (const std::string& code)
    {
        run_code<std::string::const_iterator>(code.begin(), code.end());
    }


};

void print_ast (ast::method_def& stmt) //bootstrapper::mdef<std::string::const_iterator>::toplevel_stmt& stmt)
{
    //boost::apply_visitor(ast_print(), stmt);
    Print(code_repr()(stmt));
}
void print_ast (ast::expr& stmt)
{
    Print(code_repr()(stmt));
}


}
