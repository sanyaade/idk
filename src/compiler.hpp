#pragma once

#include "ast.hpp"
#include "obj.hpp"

const oop_handle& find_global(const char* name)
{
  return oop_space::get_instance()->globals(name);
}



oop_handle new_method (oop_handle name)
{
    auto p = alloc_slots(oop_method_slots::slots, find_global("Method"));
    p->set_field(oop_method_slots::name, name);
    return oop_handle(p);
}

bool copy_str(const std::string& str, obj_ptr obj)
{
    assert(!obj.is_null());
    assert(obj->is_bitarray());
    assert(obj->get_size() >= str.size()+1);
    std::memcpy(&obj->bytes[0], str.c_str(), str.size()+1);
    return true;
}

oop_handle new_string(const std::string& str, const oop_handle klass)
{
    obj_ptr o = alloc_bits(str.size()+1, klass);
    assert(copy_str(str, o));
    assert(o->bytes[str.size()] == '\0');
    //o->bytes[name.size()] = '\0';
    //set class to symbolclass
    //o->set_class(klass);
    return oop_handle(o);
}

const oop_handle& symbols_array()
{
    return oop_space::get_instance()->sym_class().get_obj()->get_field(oop_class_slots::slots + 1);
}
oop_handle new_symbol(const std::string& name)
{
    int idx;
    // if( symbols_array().is_nil())
    // {
    //     map_t::const_iterator it = symbols.find(name);
    //     if(it != symbols.end())
    //     {
    //         return it->second;
    //     }

    // }
    // else
    // {
    idx = ordered_search(symbols_array(), name.c_str());
    if(idx > 0 && idx <= symbols_array().get_obj()->num_slots())
    {
        const auto& s = symbols_array().get_obj()->get_field(idx);
        if(cmp_symbol( s.get_obj()->cstr(), name.c_str() ) == 0)
            return s;
    }
    // }

    const auto& symcls = oop_space::get_instance()-> sym_class();
    oop_handle result = new_string(name, symcls);
    // if(symbols_array().is_nil())
    //     symbols.insert(std::make_pair(name, result));
    // else
    symcls.get_obj()->set_field(
        oop_class_slots::slots+1,
        array_insert(symbols_array(), idx, result));

    return result;
}

template<typename Iter>
oop_handle new_bytearray (Iter start, Iter end, const oop_handle& klass)
{
    std::size_t len = end - start;
    obj_ptr res = alloc_bits(len, klass);
    //res->set_field(0, klass);
    std::copy(start, end, &res->bytes[0]);
    //std::memcpy(&res->bytes[0], start, len);
    return oop_handle(res);
}


oop_handle new_process (const oop_handle& context)
{
    auto p = alloc_slots(process::slots, find_global("Process"));
    p->set_field(process::context, context);
    return oop_handle(p);
}

oop_handle new_array(std::size_t slots, const oop_handle& klass)
{
    obj_ptr res = alloc_slots(slots, klass);
    for(std::size_t i = 1; i <= slots; i++)
    {
        res->set_field(i, oop_handle::nil_obj);
    }
    return oop_handle(res);
}
oop_handle new_dict()
{
    obj_ptr res = alloc_slots(2);
    res->set_field(0, find_global("Dictionary"));
    res->set_field(1, new_array(0, find_global("OrderedArray")));
    res->set_field(2, new_array(0, find_global("Array")));
    return oop_handle(res);
}

oop_handle define_method (ast::method_def& mdef)
{
    oop_handle klass = find_global(mdef.class_name.c_str());
    //boost::apply_visitor(method_compiler(klass), mdef);
    if(klass.is_nil())
    {
        PrintError("Class not found: " << mdef.class_name);
        return oop_handle::nil_obj;
    }

    oop_handle mclass = find_global("Method");
    if(mclass.is_nil())
    {
        PrintError("Method class does not exist yet!");
        return oop_handle::nil_obj;
    }

    auto sym = new_symbol(mdef.selector);
    auto meth = new_method(sym);

    // obj_ptr new_method_p = alloc_slots(oop_method_slots::_num_slots, mclass);
    // if(new_method_p.is_null())
    // {
    //     PrintError("failed to allocate Method " << mdef.selector);
    //     return;
    // }
    // new_method_p->set_field(oop_method_slots::name, sym);

    // oop_handle meth = oop_handle(new_method_p);

    auto ctx = new vm::block_builder(meth, klass);
    ctx->get_locals().insert(ctx->get_locals().end(), mdef.args.begin(), mdef.args.end());
    ctx->get_locals().insert(ctx->get_locals().end(), mdef.locals.begin(), mdef.locals.end());
    auto visitor = vm::codegen(ctx);
    visitor.visit_statements(mdef.stmts);
    // auto it = mdef.stmts.begin();
    // auto last = mdef.stmts.end();
    // while(it < last)
    // {
    //     boost::apply_visitor(visitor, *it);
    //     it++;
    //     if(it != last) ctx->pop();
    //     //if it != last { pop }
    // }
    ctx->method_end();

    Print("defining " <<
        mdef.selector << " on " << mdef.class_name << ": " <<
        klass.repr()
    );

    oop_handle bytecodes = new_bytearray(
        ctx->get_bytes().begin(), ctx->get_bytes().end(),// + ctx->get_bytes().size(), //end(),
        find_global("ByteArray"));
    meth.get_obj()->set_field(oop_method_slots::bytecodes, bytecodes);

    oop_handle methods = klass.get_obj()->get_field(oop_class_slots::methods);
    if(methods.is_nil())
    {
        std::cout << "warning: creating methods on " << mdef.class_name << std::endl;
        methods = new_dict();
        klass.get_obj()->set_field(oop_class_slots::methods, methods);
    }
    else if(methods.get_obj()->get_class().is_nil())
    {
        std::cout << "warning: methods class was nil " << mdef.class_name << std::endl;
        methods.get_obj()->set_field(0, find_global("Dictionary"));
    }

    meth.get_obj()->set_field(oop_method_slots::locals, oop_handle(mdef.args.size() + mdef.locals.size()));
    meth.get_obj()->set_field(oop_method_slots::code,
        new_string(code_repr()(mdef), find_global("String")));
    meth.get_obj()->set_field(oop_method_slots::klass, klass);

    // PrintCode( bytecodes.get_obj().get_offset() );
    // PrintCode( bytecodes.get_obj()->get_refcount() );

    // PrintCode( meth.get_obj()->get_refcount() );

    dict_insert(
        methods,//mclass.get_obj()->get_field(oop_class_slots::methods),
        meth.get_obj()->get_field(oop_method_slots::name),
        meth);

    // PrintCode( meth.get_obj()->get_refcount() );
    return meth;

}



struct printer
{
    typedef boost::spirit::utf8_string string;

    void element(string const& tag, string const& value, int depth) const
    {
        for (int i = 0; i < (depth*4); ++i) // indent to depth
            std::cout << ' ';

        std::cout << "tag: " << tag;
        if (value != "")
            std::cout << ", value: " << value;
        std::cout << std::endl;
    }
};

void print_info(boost::spirit::info const& what)
{
    using boost::spirit::basic_info_walker;

    printer pr;
    basic_info_walker<printer> walker(pr, what.tag, 0);
    boost::apply_visitor(walker, what.value);
}

using toplevel_stmts =  std::vector< ast::toplevel_stmt>;

template<typename Iter>
bool parse_toplevel(Iter start, Iter end, toplevel_stmts& out)
{
    using Skip = boost::spirit::ascii::space_type;
    using mdef = method_def<Iter, Skip>;

    mdef gram;
    qi::rule<Iter, toplevel_stmts(), Skip> grammar =
      (gram.stmt % qi::lit('.')) >> -qi::lit('.');

    out.resize(0);
    try{
        if(!boost::spirit::qi::phrase_parse(start, end, grammar, boost::spirit::ascii::space, out))
            return false;
    }
    catch(const boost::spirit::qi::expectation_failure<std::string::const_iterator>& e)
    {
        // Print("incomplete expression")
        std::cout << "expected: ";
        print_info(e.what_);
        std::cout << "got: '"
        << std::string(e.first, e.last) << '\'' << std::endl;
        return false;
    }
    return true;

}


oop_handle new_bits(const char* bytes, int size, const oop_handle& klass)
{
    obj_ptr o = alloc_bits(size, klass);
    std::memcpy(&o->bytes[0], bytes, size);
    return oop_handle(o);
}

oop_handle create_method_context (const oop_handle& self, const oop_handle& method, const oop_handle& caller)
{
    auto sp = oop_space::get_instance();
    auto p = alloc_slots(context::slots, sp->globals("Context"));
    const oop_handle& array_class = sp->globals("Array");
    // p->set_field(0, dict_search(sp->globals(), "Context"));
    p->set_field(context::ip, oop_handle((INT_TYPE)0));
    p->set_field(context::sp, oop_handle((INT_TYPE)0));
    p->set_field(context::stack,
        oop_handle(alloc_slots(32, array_class)));
    p->set_field(context::caller, caller);
    p->set_field(context::method, method);

    auto locals = method.get_obj()->get_field(oop_method_slots::locals).get_int();
    p->set_field(context::locals, oop_handle(alloc_slots(locals + 1, array_class)));
    p->get_field(context::locals).get_obj()->set_field(locals+1, self);

    return oop_handle(p);
}

oop_handle compile_anonymous_method_process (ast::expr& expression)
{
    //make a codegen context, compile a method and prepare a process
    auto space = oop_space::get_instance();
    auto methp = alloc_slots(oop_method_slots::slots, space->globals("Method") );
    methp->set_field(oop_method_slots::name, new_bits("\0", 0, space->globals("String") ));
    auto meth = oop_handle(methp);

    auto ctx = new vm::block_builder(meth, space->globals("Object"));
    auto visitor = vm::codegen(ctx);
    boost::apply_visitor(visitor, expression);
    ctx->method_end();

    oop_handle bytecodes = new_bits(
        reinterpret_cast<const char*>(&ctx->get_bytes()[0]),
        ctx->get_bytes().size(),
        space->globals("ByteArray"));
    methp->set_field(oop_method_slots::bytecodes, bytecodes);

    //create new process
    auto context = create_method_context(oop_handle::nil_obj, meth, oop_handle::nil_obj);
    auto proc = alloc_slots(process::slots, space->globals("Process"));
    proc->set_field(process::context, context);
    return oop_handle(proc);
}
