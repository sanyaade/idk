#pragma once

#ifdef DynamicPrint
static int print_level = 1;
#define PrintN(x,n) (((n)<=print_level)? (DoPrint(x)) : std::cout)
#define Print0(x) PrintN(x,0)
#define Print1(x) PrintN(x,1)
#define Print2(x) PrintN(x,2)
#define Print3(x) PrintN(x,3)
#else
#define Print0(x) DoPrint(x)
#define Print1(x) 
#define Print2(x) 
#define Print3(x) 
#endif

#define DoPrint(x) std::cout << x << std::endl
#define Print(x) Print2(x)
#define PrintCode(x) Print3(#x ": " << x)
#define PrintWarn(x) Print1("[WARNING] " << x)
#define PrintError(x) Print0("[ERROR] " << x)
