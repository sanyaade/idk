#pragma once
#include "compiler.hpp"
#include "grammar.hpp"

namespace interp
{
struct evaluator
{
  struct visitor: boost::static_visitor<oop_handle>
  {
    oop_handle operator() (ast::method_def& m) const
    {
      define_method(m);
    }
    oop_handle operator() (ast::expr& x) const
    {
      //make a codegen context and run the expression in a VM
        
      auto proc = compile_anonymous_method_process(x);
      VM vm(oop_space::get_instance(), proc);
      //execute
      vm.run();
      return vm.result_value();
    }
  };

  static oop_handle eval (ast::toplevel_stmt& stmt)
  {
    return boost::apply_visitor(visitor(), stmt);
  }

  static oop_handle eval (const std::string& str)
  {
    toplevel_stmts s;
    if(!parse_toplevel(str.begin(), str.end(), s))
      return oop_handle::nil_obj;
    visitor v;
    oop_handle o;
    for(auto& stmt: s)
      o = eval(stmt); // boost::apply_visitor(v, stmt);
    return o;
  }

  static bool parse_markdown_code_sections (const std::string& filename, std::vector<std::string>& out)
  {
    using Iter = std::string::const_iterator;
    md_parser<Iter> g;
    std::ifstream f(filename);
    std::stringstream buf;
    buf << f.rdbuf();
    Iter beg = buf.str().begin(), end = buf.str().end();
  
    try{
      if(!boost::spirit::qi::parse(beg,end, g, out))
        return false;
    }
    catch(const boost::spirit::qi::expectation_failure<std::string::const_iterator>& e)
    {
      // Print("incomplete expression")
      std::cout << "expected: ";
      print_info(e.what_);
      std::cout << "got: '"
      << std::string(e.first, e.last) << '\'' << std::endl;
      return false;
    }
    return true;
  }

  static oop_handle eval_file (const std::string& file)
  {
    std::vector<std::string> code;
    if(file.substr(file.size() - 3, 3) == ".md")
    {
      parse_markdown_code_sections(file, code);
    }
    else
    {
      std::ifstream f(file);
      std::stringstream buf;
      buf << f.rdbuf();
      code.push_back(buf.str());
    }

    oop_handle x;
    for(auto& s: code)
      x = eval(s);
    return x;
  }
};
}
