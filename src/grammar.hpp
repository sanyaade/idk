#pragma once

#include <boost/config/warning_disable.hpp>
#define BOOST_SPIRIT_DEBUG
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/foreach.hpp>
#include <boost/variant.hpp>



#include "ast.hpp"

struct print: boost::static_visitor<>
{
    void operator() (const std::string& s) const
    {
        std::cout << s << std::endl;
    }
    void operator() (const ast::expr& x) const
    {
        boost::apply_visitor(ast_print(), const_cast<ast::expr&>(x));
        // ast_print()(x);
    }
};

using frag = boost::variant<std::string, ast::expr>;
ast::expr collect_kwds(const std::vector<frag> & v)
{
    //collect arguments from {x, at:, y, put:, z} to (at:put: x y z)
    if(v.size() == 1) return boost::get<ast::expr>(v[0]);
    if(!(v.size() & 1))
    {
        std::cout << v.size() << std::endl;
        for(auto& it: v)
        {
            boost::apply_visitor(print(), it);
            std::cout << std::endl;
        }
    }
    assert(v.size() & 1);
    ast::message m;
    m.args.push_back(boost::get<ast::expr>(v[0]));
    for(int i = 1; i < v.size(); i+=2 )
    {
        const auto s = boost::get<std::string>(&v[i]);
        const auto exp = boost::get<ast::expr>(&v[i+1]);
        assert(s);
        assert(exp);

        m.name.append(*s);
        m.args.push_back(*exp);
    }

    return m;
}

ast::expr collect_binops(const std::vector<frag>& v)
{
    // do a left fold to turn {x, +, y, -, z} into (- (+ x y) z)
    auto res = boost::get<ast::expr>(v[0]);
    if(v.size() == 1) return res;
    assert(v.size() & 1);
    for(int i = 1; i < v.size(); i+=2)
    {
        const auto s = boost::get<std::string>(&v[i]);
        const auto exp = boost::get<ast::expr>(&v[i+1]);
        assert(s);
        assert(exp);
        ast::message m;
        m.name = *s;
        m.args.push_back(res);
        m.args.push_back(*exp);
        res = m;
    }
    return res;
}

ast::expr collect_unary (const ast::expr& accum, const std::vector<std::string>& msgs)
{
    ast::expr result = accum;
    for(const auto& m: msgs)
        result = ast::message {m, {result}};
    return result;
}















namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;

template<typename It, typename Skip = ascii::space_type>
struct md_parser: qi::grammar<It, std::vector<std::string>()>//, Skip>
{
  md_parser(): md_parser::base_type(start)
  {
    using namespace boost::spirit::qi;
    using namespace boost::spirit;

    code_section %=
      lit("```fowltalk") >> eol >>
      *(!lit("```") >> line) >> // *(char_ - eol) >> eol) >>
      lit("```") >> eol;
    line %= *(!eol >> char_) >> eol;

    start %= +(code_section | omit[ line ]);
  }

  template<typename...Args>
  using R = qi::rule<It, Args...>;
  R<std::vector<std::string>()> start;//, Skip> start;
  R<std::string()> line;
  R<std::string()> code_section;
};



template<typename It, typename Skip = ascii::space_type>
struct g2: qi::grammar<It, ast::expr(), Skip>
{

    template<typename...Args>
    using R = qi::rule<It, Args...>;
    R<void()> comment;

    R<ast::expr(), Skip>
        start, expr_, term_, paren_expr,
        binop_term, unary_term, kwd_expr, stmt_,
        binexp5, binexp4, binexp3, binexp2, binexp1;
    R<ast::symbol()> sym_;
    R<ast::intlit()> int_;
    R<ast::nil()> nil_;
    R<ast::truelit()> true_;
    R<ast::falselit()> false_;
    R<ast::self()> self_;
    R<ast::super()> super_;
    R<ast::thisContext()> thiscontext_;
    R<ast::constant()> const_;
    R<ast::variable()> var_, instvar_;
    R<ast::stringlit()> strlit_;
    R<ast::block(), Skip> block_;
    R<ast::assign(), Skip> assign_expr;
    R<ast::array(), Skip> array_expr;
    R<ast::call_primitive(), Skip> call_prim_;
    R<std::vector<ast::expr>(), Skip> toplevel_stmts;

    R<ast::ret(), Skip> ret_;

    R<char()> str_char, escaped_char;
    qi::symbols<char const, char> escaped_chars;

    R<void()> reserved_ident;
    R<void()> ident_char;

    R<boost::fusion::vector<ast::expr, std::vector<std::string>>(), Skip> unary_part;
    R<std::string()>
        kwd_str, ident_str, const_str, sym_str,
        block_input, block_local, unary_selector, binop_op,
        op1, op2, op3, op4, op5, binary_op, str_chars, str_interp_fragment_str;

    using frag = boost::variant<
                 std::string, ast::expr >;
    R<std::vector<frag>(), Skip> kwd_msg, binop_msg,
        binop1, binop2, binop3, binop4, binop5;//, unary_selector;


    R<frag()>
        op_mult, op_additive, op_shift, op_compare, op_eq;

    R<frag()> msg_part, binop_part, str_interp_fragment;
    R<std::vector<frag>()> str_interp_;

    g2(): g2::base_type(start)
    {
        using namespace boost::spirit::qi;
        namespace ph = boost::phoenix;

        start = expr_.alias();

        comment = qi::string("//") >> *(!boost::spirit::eol >> char_) >> boost::spirit::eol;
        kwd_str = alpha >> *(alnum | char_('_')) >> char_(':') ;

        ident_str = lower >> *(alnum | char_('_')) >> !lit(':') ;
        const_str = upper >> *(alnum | char_('_')) > !lit(':') ;
        sym_str %= hold[ +kwd_str ] | hold[ const_str ] | ident_str ; //[_val = qi::_1];

        sym_ %= '#' >> lexeme[ sym_str ][ ph::at_c<0>(_val) = qi::_1 ];//qi::_val = boost::phoenix::static_cast_<ast::symbol>(qi::_1)];
        int_ %= qi::int_ [ ph::at_c<0>(_val) = qi::_1 ];

        ident_char = qi::char_("A-Za-z0-9") | lit(':') | lit('_');
        nil_ = (qi::lit("nil") >> !ident_char) [ _val = ph::construct<ast::nil>() ];
        true_ = (qi::lit("true") >> !ident_char) [ _val = ph::construct<ast::truelit>() ];
        false_ = (qi::lit("false") >> !ident_char) [ _val = ph::construct<ast::falselit>() ];
        self_ = (qi::lit("self") >> !ident_char) [ _val = ph::construct<ast::self>() ];
        super_ = (qi::lit("super") >> !ident_char) [ _val = ph::construct<ast::super>() ];
        thiscontext_ = (qi::lit("thisContext") >> !ident_char)[ _val = ph::construct<ast::thisContext>() ];

        paren_expr %= '(' > expr_ > ')';
        array_expr %=
            lit("#(")
            >> -((expr_ % '.') >> -lit('.'))
            >> lit(")");
        const_ %= const_str;
        var_ %= !reserved_ident >> ident_str;
        instvar_ %= lit('@') >> !reserved_ident >> ident_str;

        reserved_ident = (
            nil_ | true_ | false_ | self_ |
            //(lit("super") >> !ident_char) |
            super_ |
            (lit("def") >> !ident_char)
        );

        escaped_chars.add
        ("n", '\n')
        ("'", '\'')
        ("\\", '\\')
        ("\"", '"');
        escaped_char = lit('\\') > escaped_chars;

        str_char = (char_ - (lit('\'') | lit('\\'))) | escaped_char;
        str_chars %=
            *str_char;
        strlit_ =
            lit('\'') >> str_chars >> lit('\'');
        // str_interp_fragment_str =
        //     +(!lit("(") >> ( (char_ - (lit('\"') | lit('\\'))) | escaped_chars));
        // str_interp_fragment %=
        //     (lit("#(") >> expr_ >> lit(')')) |
        //     str_interp_fragment_str;
        // str_interp_ %=
        //     lit('"') >>
        //     + ( !lit('"') >> str_interp_fragment ) >>
        //     lit('"');
        term_ %=
            nil_ | true_ | false_
            | self_ | super_ | thiscontext_
            | instvar_ | const_ | var_ | int_ | strlit_
            | array_expr | sym_
            | paren_expr | block_;

        msg_part %= lexeme[kwd_str] ;

        op1 = qi::char_("*/%");
        op2 = qi::char_("+-");
        op3 = qi::string("<<") | qi::string(">>");
        op4 =
            qi::string("<=") | qi::string(">=") |
            qi::hold[ (qi::char_('<') >> !qi::lit('<')) ] |
            qi::hold[ (qi::char_('>') >> !qi::lit('>')) ];
        op5 =
            qi::string("!=") | qi::string("==");

        binary_op = op5 | op4 | op3 | op2 | op1;

        op_mult %=  op1;
        op_additive %= op2;
        op_shift %= op3;
        op_compare %= op4;
        op_eq %= op5;

        binop_op = qi::string("<<") | qi::string(">>") | lexeme[ char_("+/*%,$&<>") | char_('-') ];
        binop_part %= binop_op;
        unary_selector %= ident_str ;

        //unary_msg %= ;
        unary_part = term_ >> *unary_selector; //(term_ >> *unary_selector);// [ what_is_the_attr() ];
        unary_term = unary_part[ _val = ph::bind(&collect_unary, ph::at_c<0>(qi::_1), ph::at_c<1>(qi::_1)) ];//unary_part[ what_is_the_attr() ] ; //_val = ph::at_c<1>(qi::_1) ]; //term_[ _val = qi::_1 ] >> (*unary_selector)[ _val = ph::bind(&collect_unary, _val, qi::_1) ];

        //unary_term = unary_part [ _val = ph::bind(&collect_unary, ph::at_c<0>(qi::_1), ph::at_c<1>(qi::_1)) ];

        binop1  = (unary_term >> *(op_mult > unary_term));
        binexp1 = binop1[ _val = ph::bind(&collect_binops, qi::_1) ];
        binop2  = (binexp1 >> *(op_additive > binexp1));
        binexp2 = binop2[ _val = ph::bind(&collect_binops, qi::_1) ];
        binop3  = (binexp2 >> *(op_shift > binexp2));
        binexp3 = binop3[ _val = ph::bind(&collect_binops, qi::_1) ];
        binop4  = (binexp3 >> *(op_compare > binexp3));
        binexp4 = binop4[ _val = ph::bind(&collect_binops, qi::_1) ];
        binop5  = (binexp4 >> *(op_eq > binexp4));
        binexp5 = binop5[ _val = ph::bind(&collect_binops, qi::_1) ];

        //binop_msg = unary_term >> *(binop_part > unary_term);
        //binop_term = binop_msg [ _val = ph::bind(&collect_binops, qi::_1) ];
        binop_term = binexp5;

        kwd_msg =
            binop_term[ ph::push_back(_val, qi::_1) ] >>
            -(msg_part[ ph::push_back(_val, qi::_1) ] >>
            binop_term[ ph::push_back(_val, qi::_1) ] >>
            *(  msg_part[ ph::push_back(_val, qi::_1) ] >>
                binop_term[ ph::push_back(_val, qi::_1) ]
            ));
        kwd_expr = kwd_msg[ _val = ph::bind(&collect_kwds, qi::_1) ];

        assign_expr = lexeme[-qi::lit("@") >> ident_str]
            >> (lit(":=") | lit('=') | lit("<-"))
            >> kwd_expr;
        expr_ = kwd_expr ;

        call_prim_ =
            lit("<")
            >> qi::int_
            >> *term_
            >> lit(">");
        ret_ = lit("^") >> expr_;
        stmt_ =
            assign_expr | ret_ | call_prim_ | expr_;

        block_input %= lit(':') >> lexeme[ident_str];
        block_local %= lexeme[ident_str];

        block_ =
            lit('[')
            >> -( (&lit(':') | lit('|'))
                 >> *(block_input[ ph::push_back(ph::at_c<0>(_val), qi::_1) ])
                 >> *(block_local[ ph::push_back(ph::at_c<1>(_val), qi::_1) ])
                 >> lit('|') )
            >> -comment
            >> (stmt_ % (lit('.') >> -comment)) [ ph::at_c<2>(_val) = qi::_1 ]
            >> -lit('.')
            >> lit(']');



        toplevel_stmts =
            (expr_ % lit('.'))
            >> -lit('.');

        msg_part.name("keyword");
        expr_.name("expr");
        int_.name("int");
        sym_.name("symbol");
        nil_.name("nil");
        true_.name("true");
        false_.name("false");
        self_.name("self");
        block_.name("block");
        strlit_.name("string");
        array_expr.name("array");
        call_prim_.name("call primitive");
        var_.name("load variable");
        const_.name("load const");

        kwd_expr.name("kwd message");
        binop_term.name("binary message");
        unary_term.name("nullary message");
        term_.name("terminal");





        on_error<fail>
        (expr_,
         std::cout << ph::val("Expected ") << qi::_4
         << ph::val(" here: \"") << ph::construct<std::string>(qi::_3, qi::_2)
         << ph::val("\" just after: \"") << ph::construct<std::string>(qi::_3 - 5, qi::_3)
         << ph::val("\"") << std::endl
        );

        //BOOST_SPIRIT_DEBUG_NODES((start)(expr_)(block_)(int_)(sym_)(msg_part))
        /*debug(binop_term);
        debug(term_);
        debug(block_);
        debug(var_);*/
    }
};





template<
    typename It,
    typename Skip = ascii::space_type
>
class method_def: public qi::grammar<It, ast::method_def(), Skip>
{
public:
    template<typename... Args>
    using R = qi::rule<It, Args...>;

    R<ast::method_def(), Skip>
        start;
    R<boost::fusion::vector< std::string, std::vector<std::string> >(), Skip >
        method_header, binop_mheader, nullary_mheader, keyword_mheader;

    g2<It, Skip> expr;
    using toplevel_stmt = ast::toplevel_stmt;
    R<toplevel_stmt(), Skip> stmt;

    method_def(): method_def::base_type(start)
    {
        namespace ph = boost::phoenix;
        binop_mheader =
            expr.binary_op[ ph::at_c<0>(qi::_val) = qi::_1 ]
            >> expr.ident_str [ ph::push_back(ph::at_c<1>(qi::_val), qi::_1) ];
        nullary_mheader =
            expr.ident_str [ ph::at_c<0>(qi::_val) = qi::_1 ];
        keyword_mheader =
            +(  expr.kwd_str[ ph::insert(ph::at_c<0>(qi::_val), ph::end(ph::at_c<0>(qi::_val)), ph::begin(qi::_1), ph::end(qi::_1)) ]
                >> expr.ident_str[ ph::push_back(ph::at_c<1>(qi::_val), qi::_1) ]
            );
        method_header =
            binop_mheader | nullary_mheader | keyword_mheader;

        start =
            qi::lit("def")
            >> expr.const_str [ ph::at_c<3>(qi::_val) = qi::_1 ] // class_name
            >> method_header [
                ph::at_c<4>(qi::_val) = ph::at_c<0>(qi::_1), // selector
                ph::at_c<0>(qi::_val) = ph::at_c<1>(qi::_1) ] // args
            >> expr.block_ [
                ph::at_c<2>(qi::_val) = ph::at_c<2>(qi::_1), // stmts
                ph::at_c<1>(qi::_val) = ph::at_c<1>(qi::_1) ] // locals
        ;

        stmt = start | expr;

    }
};
