#pragma once
#include <functional>
#include <iostream>

#define DBG(s) std::cout << s << std::endl

template<typename Ptr>
class linked_list;

template<typename Ptr>
class linked_item
{
    Ptr prev, next;
    friend linked_list<Ptr>;
public:
    using ptr_t = Ptr;

    linked_item(): prev(), next() {}

    const Ptr& get_prev()
    {
        return prev;
    }
    const Ptr& get_next()
    {
        return next;
    }
};

template<typename Ptr>
class linked_list
{
    Ptr head, tail;
public:
    linked_list(): head(), tail() {}

    const Ptr &get_head() const
    {
        return head;
    }
    const Ptr &get_tail() const
    {
        return tail;
    }

    void iterate (std::function<void(Ptr)> fn)
    {
        std::cout << "head iterate ";
        auto it = get_head();
        while(it)
        {
            fn(it);
            it = it->get_next();
        }
        std::cout << "head iterate done" << std::endl;
    }
    void iterate2 (std::function<void(Ptr)> fn)
    {
        std::cout << "tail iterate ";
        auto it = get_tail();
        while(it)
        {
            fn(it);
            it = it->get_prev();
        }
        std::cout << "tail iterate done" << std::endl;
    }

    void append (const Ptr &item)
    {
        item->next = nullptr;
        item->prev = tail;
        if(tail) tail->next = item;
        if(!head)
        {
            //DBG("setting head from append!");
            head = item;
        }
        tail = item;
    }

    void prepend(const Ptr &item)
    {
        item->next = head;
        item->prev = nullptr;
        if(head) head->prev = item;
        if(!tail)
        {
            //DBG("setting trail from prepend!");
            tail = item;
        }
        head = item;
    }

    void remove(const Ptr &item)
    {
        if(head && item == head)
        {
            //DBG("removing head");
            head = item->next;
        }
        if(tail && item == tail)
        {
            //DBG("removing tail");
            tail = item->prev;
        }
        if(item->next) item->next->prev = item->prev;
        if(item->prev) item->prev->next = item->next;
        item->next = nullptr;
        item->prev = nullptr;
    }
};
