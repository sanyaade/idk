#pragma once

#ifdef EnablePrintAllocations
#   define PrintAlloc(x) Print(x)
#else
#   define PrintAlloc(x)
#endif

#define MISSING_MESSAGE_NAME "messageMissing:"

#include <cstring>
#include <cassert>
#include <algorithm>
#include <fstream>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include "defs.hpp"
#include "oop_tags.hpp"
#include "obj_header.hpp"
#include "oop_slots.hpp"
#include "linkedlist.hpp"
#include "oop_handle.hpp"

namespace interp
{
class bootstrapper;
}

class oop_obj:
    public oop_header<oop_obj>,
    public linked_item<obj_ptr>
{
public:
    using region = ptr_t::region_t;// region<oop_obj>;
    using handle = oop_handle; 
    using absptr_t = obj_ptr;
    using oop_header_t = oop_header<oop_obj>;

    //oop_obj(): linked_item(), oop_size(), klass() {}
    oop_obj(std::size_t size_bytes, bool is_bitarray, oop_handle klass = oop_handle::nil_obj):
        linked_item(), oop_header(size_bytes, is_bitarray)
    {
        
        //std::cout << "new obj created! " << get_size() << ' ' << size_bytes << std::endl;
        //std::cout << "new obj created: " << repr() <<
        //"("<<get_size()<<")"<<std::endl;
        set_class(klass);
        std::memset(&bytes[0], 0, size_bytes);
        if(!is_bitarray)
        {
            int nslots = size_bytes / sizeof(oop_handle);
            for(int i = size_bytes/sizeof(oop_handle); i; --i)
                set_field(i, oop_handle::nil_obj);
        }
    }

    inline std::string repr() const;
    inline int64_t identity() const;


    void clear_fields()
    {
        if(is_free() || is_bitarray())
            return;
        // assert(!is_bitarray());
        auto ns = num_slots();
        for(auto i = 0; i < ns; i++)
        {
            set_field(i, oop_handle::nil_obj);
        }
    }
    inline void free();

    friend class oop_handle;
    friend class oop_space;
    friend class interp::bootstrapper;
    friend class obj_header;

protected:


    std::size_t total_size()
    {
        return get_size() + sizeof(oop_obj);
    }

    inline void set_class(handle klass)
    {
        fields[0] = klass;
    }

    //my_abs_ptr<oop_class> klass;
    handle fields[1];
public:

    uint8_t bytes[0];

    const handle* raw_fields() const
    {
        return &fields[0];
    }

    const handle& get_class() const
    {
        return fields[0];
    }

    const handle& get_field(std::size_t n) const
    {
        assert(! is_free());
        return fields[n];
    }

    inline void set_field(std::size_t n, oop_handle obj)
    {
        assert(! is_free());
#ifndef CppRefcounting
        if(obj.is_obj()) obj.raw_get<TAG_OBJ>()->incref();
        if(fields[n].is_obj()) fields[n].raw_get<TAG_OBJ>()->decref();
#endif
        fields[n] = obj;
    }


    std::size_t num_slots() const
    {
        if(is_bitarray()) return 0;
        //if(get_class().is_nil())
        return get_size() / sizeof(oop_handle);
        //SHOWSRC(get_class().repr());
        //return get_class().get<TAG_OBJ>()->get_field(oop_class_slots::instance_size) / sizeof(oop_handle);
    }
    const char* cstr() const
    {
        return reinterpret_cast<const char*>(&bytes[0]);
    }
};


struct oop_space
{
    char* start;
    uint64_t size;
    // oop_handle globals = oop_handle::nil_obj;

private:

    friend class interp::bootstrapper;

    friend const oop_handle& oop_handle::get_class() const;

    using linked_list_t = linked_list<obj_ptr>;
    class space_header
    {
        friend class oop_space;
        friend class interp::bootstrapper;
        linked_list<obj_ptr> used, free;
    public:
        // oop_handle nil_class, true_class, false_class, int_class;
        // oop_handle special_classes;
        oop_handle image;

        // space_header()
        // {}
    private:
        template<unsigned slot>
        inline const oop_handle& get()
        {
            return image.get_obj()->get_field(slot);
        }
        template<unsigned slot>
        inline void set(const oop_handle& val)
        {
            image.get_obj()->set_field(slot, val);
        }
    public:
        inline const oop_handle& globals();
        inline const oop_handle& nil_class();
        inline const oop_handle& true_class();
        inline const oop_handle& false_class();
        inline const oop_handle& int_class();
        inline const oop_handle& sym_class();
    };

    inline space_header* this_header() const
    {
        return (space_header*)start;
    }
    inline char* data0() const
    {
        return (char*)(this_header()+1);
    }
    inline linked_list_t& free_items() const
    {
        return this_header()->free;
    }
    inline linked_list_t& used_items() const
    {
        return this_header()->used;
    }

public:

#ifdef EnableMarkAndSweep
    void mark(obj_ptr obj)
    {
        if(obj->is_marked()) return;
        obj->mark();
        for(int i= 0, h= (obj->is_bitarray() ? 0 : obj->num_slots());
            i <= h;
            ++i)
        {
            const oop_handle& o = obj->get_field(i);
            if(o.is_obj()) mark(o.raw_get<TAG_OBJ>());
        }
    }
    void mark() const
    {
        it = used_items().get_head();
        while(it)
        {
            mark(*it);
            it = it->get_next();
        }
    }
    void sweep(const oop_handle& root_object)
    {
        // auto it = used_items().get_head();
        // while(it)
        // {
        //     it->unmark();
        //     it = it->get_next();
        // }
        mark(root_object.get_obj());

        auto it = used_items().get_head();
        while(it)
        {
            if(!it->is_marked())
            {
                //
            }
            it = it->get_next();
        }
    }

#endif

    std::size_t free_space() const;

    inline const oop_handle& globals() const
    {
        return this_header()->globals();
    }
    inline const oop_handle& globals(const char* sym) const;
    inline const oop_handle& sym_class() const
    {
        return this_header()->sym_class();
    }

    bool write_file(const char* path)
    {
        std::ofstream file(path, std::ios::out | std::ios::binary);
        if(!file)
        {
            PrintError("unable to open " << path);
            return false;
        }
        auto size_left = size;
        char* addr = (char*)this_header();
        while(file.write(addr, (size_left < 1024) ? size_left : 1024))
        {
            if(size_left < 1024) break;
            addr += 1024;
            size_left -= 1024;
            if(size_left == 0) break;
        }

        return true;
    }

    static oop_space* read_file(const char* path)
    {
        struct stat results;
        if(stat(path, &results) == 0)
        {
            auto sz = results.st_size;

            char* data = new char[sz];
            oop_space* space = create(sz, data, false);

            std::ifstream file(path, std::ios::in | std::ios::binary);
            auto size_left = space->size;
            char* addr = data;

            while(file.read(addr, (size_left < 1024) ? size_left : 1024))
            {
                if(size_left < 1024) break;
                addr += 1024;
                size_left -= 1024;
                if(size_left == 0) break;
            }

            return space;
        }
        return 0;
    }

    static oop_space* mmap_image(const char* path)
    {
        struct stat results;
        if(stat(path, &results) == 0)
        {
            auto sz = results.st_size;
            auto fd = open(path, O_RDWR);


            if(fd < 0)
            {
                PrintError("failed to open " << path);
                return 0;
            }
            void* p = mmap64(0, sz, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
            if(p == MAP_FAILED) 
            {
                PrintError(strerror(errno));
                return 0;
            }

            oop_space* space = create(sz, (char*)p, false);
            return space;
        }
        return 0;
    }

    //oop_handle nil_obj() { return oop_handle::nil_obj; }

    void free (obj_ptr obj);
    obj_ptr alloc (std::size_t bytes, bool is_bitarray);

    static void setup_instance(oop_space* space);
    static oop_space* create(uint64_t size, bool init = true);
    static oop_space* create(uint64_t size, char* data, bool init = true);

    static inline oop_space* get_instance()
    {
        return oop_space::instance;
    }
private:
    static oop_space* instance;
};



std::size_t oop_space::free_space() const
{
    std::size_t sum = 0;
    auto i = free_items().get_head();
    while(i)
    {
        sum += i->get_size();
        i = i->get_next();
    }
    return sum;
}



#define new_int(num) oop_handle::mk((INT_TYPE)(num))


int cmp_symbol (const char* a, const char* b);

int cmp_symbol (const oop_handle& a, const oop_handle& b);

int class_find_instvar (const oop_handle& cls, const char* name);

int obj_find_instvar (const oop_handle& obj, const char* name);
int ordered_search (const oop_handle& array, const auto& val, auto compare);

int ordered_search (const oop_handle& array, const char* val);
const oop_handle& dict_search(const oop_handle& dict, const char* name);

oop_handle lookup_method_class(const oop_handle& klass, const char* name);
oop_handle lookup_method(const oop_handle& obj, const char* name);
// inline const oop_handle& oop_space::globals(const char* sym) const;

oop_handle array_insert (const oop_handle& array,
                         int index,
                         const oop_handle& val);

bool dict_insert (const oop_handle& dict,
                  const oop_handle& key,
                  const oop_handle& val);



static const int alloc_fail = 42;

obj_ptr alloc_slots (std::size_t slots, const oop_handle& klass = oop_handle::nil_obj);
obj_ptr alloc_bits (std::size_t bytes, const oop_handle& klass = oop_handle::nil_obj);









template<>
uintptr_t oop_obj::region::base(0);

const oop_handle oop_handle::true_obj  = oop_handle::mk<TAG_TRUE>();
const oop_handle oop_handle::false_obj = oop_handle::mk<TAG_FALSE>();
const oop_handle oop_handle::nil_obj   = oop_handle::mk<TAG_NIL>();

oop_handle::~oop_handle()
{
#ifdef CppRefcounting
    if(is_obj()) raw_get<TAG_OBJ>()->decref();
#endif
}

int64_t oop_obj::identity() const
{
    return obj_ptr(this).get_offset();
}
std::string oop_obj::repr() const
{
    return std::string("<") + (get_class().is_nil() ? "nil" : get_class().get_obj()->get_field(oop_class_slots::name).get_obj()->cstr() ) + 
        ":"+std::to_string(this->identity()) +" "+
        (is_bitarray()
            ? std::string(this->cstr())
            : "") +
        ">"; //"<Object:"+std::to_string(this->identity()) +">";
}

void oop_obj::free()
{
    assert(! is_free());
    PrintAlloc("** freeing " << repr() <<
        " (" << get_class().repr() <<") " << 
        (is_bitarray() ? get_size() : num_slots()));
    oop_space::get_instance()->free(obj_ptr(this));
    assert(is_free());
}

oop_handle::oop_handle(const oop_handle& other): tag_t(tag_t::copy(other))
{
#ifdef CppRefcounting
    if(is_obj()) raw_get<TAG_OBJ>()->incref();
#endif
}

oop_handle::oop_handle(const my_abs_ptr< oop_obj >& ptr): oop_handle::tag_t(ptr)
{
    if(ptr.is_null())
    {
        tag_t::reset(TAG_NIL);
        return;
    }
    assert(!ptr.is_null());
#ifdef CppRefcounting
    ptr->incref();
#endif
}

oop_handle& oop_handle::operator= (const oop_handle& other)
{
#ifdef CppRefcounting
    if(is_obj()) raw_get<TAG_OBJ>()->decref();
#endif
    bits = other.bits;
#ifdef CppRefcounting
    if(is_obj()) raw_get<TAG_OBJ>()->incref();
#endif
    return *this;
}

const oop_handle& oop_handle::get_class() const
{
    auto tag = get_tag();
    switch(tag)
    {
    case TAG_OBJ:
        return get<TAG_OBJ>()->get_class();
    case TAG_INT:
        return oop_space::instance->this_header()->int_class();
    case TAG_NIL:
        return oop_space::instance->this_header()->nil_class();
    case TAG_TRUE:
        return oop_space::instance->this_header()->true_class();
    case TAG_FALSE:
        return oop_space::instance->this_header()->false_class();
    default:
        return oop_space::instance->this_header()->nil_class();
    }
}


std::string oop_handle::repr () const
{
    auto tag = get_tag();
    switch(tag)
    {
    case TAG_INT:
        return std::to_string(this->get<TAG_INT>());
    case TAG_OBJ:
        return get<TAG_OBJ>()->repr();// "<Object:"+std::to_string(this->get<TAG_OBJ>().get_offset()) +">";
    case TAG_NIL:
        return "nil";
    case TAG_FALSE:
        return "false";
    case TAG_TRUE:
        return "true";

    default:
        return "???";
    }
}

#define new_int(num) oop_handle::mk((INT_TYPE)(num))
#ifdef CppRefcounting
template<class Self>
void oop_header<Self>::incref()
{
    //std::cout << "* incref " << static_cast<Self*>(this)->repr() <<
    //" (" << static_cast<Self*>(this)->get_class().repr() << ")"<< std::endl;
    assert(!is_free());
    refcount++;
}
#endif

// obj_ptr oop_handle::get_obj() const

int cmp_symbol (const char* a, const char* b)
{
    return std::strcmp(a, b);
}

int cmp_symbol (const oop_handle& a, const oop_handle& b)
{
    return std::strcmp(a.get_obj()->cstr(), b.get_obj()->cstr());
}

int class_find_instvar (const oop_handle& cls, const char* name)
{
    int low;
    const auto& parent = cls.get_obj()->get_field(oop_class_slots::superclass);
    if(parent.is_obj())
    {
        auto res = class_find_instvar(parent, name);
        if(res >= 0) return res;
        low = 1+ parent.get_obj()->get_field(oop_class_slots::instance_size).get_int();
    }
    else
        low = 1;

    const auto& ivs = cls.get_obj()->get_field(oop_class_slots::variables);
    //SHOWSRC( ivs.repr() );
    const auto ivsize = ivs.is_nil() ? 0 : ivs.get_obj()->num_slots();

    //std::cout << "-- looking for " << name << std::endl;

    for(auto i = 1; i <= ivsize; i++)
    {
        const char* cs = ivs.get_obj()->get_field(i).get_obj()->cstr();
        //std::cout << "  " << cs << std::endl;

        if(std::strcmp(cs, name) == 0)
            return low;
        low++;
    }
    return -1;
}

int obj_find_instvar (const oop_handle& obj, const char* name)
{
    return class_find_instvar(obj.get_class(), name);
}

// def OrderedArray location: value
// [|low high mid|
//     low := 1.
//     high := self size + 1.
//     [ low < high ] whileTrue: [
//         mid := (low + high) / 2.
//         (self at: mid) < value
//             ifTrue: [ low := mid + 1 ]
//             ifFalse: [ high := mid ].
//     ].
//     low
// ].

int ordered_search (const oop_handle& array, const auto& val, auto compare)
{
    int low = 1;
    int high= 1+array.get_obj()->num_slots();
    // Print(" searching for " << val << " high=" << high);
    while(low < high)
    {
        int mid = (low + high) / 2;
        int res = compare(array.get_obj()->get_field(mid), val);
        // Print("  " << array.get_obj()->get_field(mid).get_obj()->cstr() << " " << res);
        if(res == 0)
            return mid;
        if(res < 0)
            low = mid+1;
        else
            high = mid;
    }
    return low;
}

int ordered_search (const oop_handle& array, const char* val)
{
    return ordered_search(array, val, [](const oop_handle& H, const char* L){
        return cmp_symbol(H.get_obj()->cstr(), L);
    });
}

const oop_handle& dict_search(const oop_handle& dict, const char* name)
{
    const auto& keys = dict.get_obj()->get_field(1);
    int idx = ordered_search( keys, name);
    if(idx > 0 && idx <= keys.get_obj()->num_slots())
    {
        auto res = cmp_symbol(keys.get_obj()->get_field(idx).get_obj()->cstr(), name);
        if(res == 0) return dict.get_obj()->get_field(2).get_obj()->get_field(idx);
    }
    return oop_handle::nil_obj;
}

oop_handle lookup_method_class(const oop_handle& klass, const char* name)
{
    
    for(oop_handle kls= klass;
        ! kls.is_nil();
        kls = kls.get_obj()->get_field(oop_class_slots::superclass))
    {
        const oop_handle
            & cls_name = kls.get_obj()->get_field(oop_class_slots::name),
            & methods = kls.get_obj()->get_field(oop_class_slots::methods);
        if(methods.is_nil())
        {
            PrintWarn("class " << cls_name.get_obj()->cstr() 
                  << " methods is nil");
            continue;
        }
        const oop_handle& res = dict_search(methods, name);

        if(! res.is_nil()) return res;//methods.get_obj()->get_field(2);
        Print("** " << name << " not found in " << cls_name.get_obj()->cstr());
    }
    return oop_handle::nil_obj;
}

oop_handle lookup_method(const oop_handle& obj, const char* name)
{
    return lookup_method_class(obj.get_class(), name);
}

inline const oop_handle& oop_space::globals(const char* sym) const
{
    return dict_search(globals(), sym);
}

oop_handle array_insert (const oop_handle& array,
                         int index,
                         const oop_handle& val)
{   //returns a new array with the same class as `array`

    obj_ptr res = oop_space::get_instance()->alloc(
        sizeof(oop_handle)*(1 + array.get_obj()->num_slots()),
        false);
    res->set_field(0, array.get_class());

    int i = 1;
    for(; i < index; ++i)
        res->set_field(i, array.get_obj()->get_field(i));
    res->set_field(i, val);
    auto array_size = array.get_obj()->num_slots();
    for(; i <= array_size; ++i)
        res->set_field(i+1, array.get_obj()->get_field(i));

    return oop_handle(res);
}

bool dict_insert (const oop_handle& dict,
                  const oop_handle& key,
                  const oop_handle& val)
{
    const oop_handle& keys = dict.get_obj()->get_field(1);
    const oop_handle& vals = dict.get_obj()->get_field(2);
    int highest = keys.get_obj()->num_slots();
    // Print(":: " << key.get_obj()->repr() << " highest=" << highest);
    int idx = ordered_search(keys, key.get_obj()->cstr());
    // Print(idx);
    if(idx > 0)
    {
        if(idx < keys.get_obj()->num_slots()+1 && 
          0 == cmp_symbol(key.get_obj()->cstr(), keys.get_obj()->get_field(idx).get_obj()->cstr()))
        {
            vals.get_obj()->set_field(idx, val);
        }
        else
        {
            dict.get_obj()->set_field(1, array_insert(keys, idx, key));
            dict.get_obj()->set_field(2, array_insert(vals, idx, val));
            return true;
        }
    }
    return false;
}


obj_ptr alloc_slots (std::size_t slots, const oop_handle& klass)
{
    obj_ptr result = oop_space::get_instance()->alloc(slots * sizeof(oop_handle), false);
    if(!result) throw(alloc_fail);

    PrintAlloc("** alloced " << slots << " slots at " << result.get_offset());
    result->set_field(0, klass);//oop_handle::nil_obj);

    return result;
}


obj_ptr alloc_bits (std::size_t bytes, const oop_handle& klass)
{
    obj_ptr res = oop_space::get_instance()->alloc(bytes, true);
    if(!res) throw(alloc_fail);

    PrintAlloc("** alloced " << bytes << " bytes at " << res.get_offset());
    res->set_field(0, klass);
    return res;
}




//oop_space stuff

oop_space* oop_space::instance = nullptr;

void oop_space::free (obj_ptr obj)
{
    assert(! obj->is_free());

    used_items().remove(obj);
    obj->set_free();
    obj->clear_fields();
    //pad size
    constexpr auto sizeof_handle = sizeof(oop_handle);
    const auto padded_size = (obj->get_size() % sizeof_handle)
        ? (sizeof_handle - (obj->get_size() % sizeof_handle))
        : 0;
    obj->set_size(obj->get_size() + padded_size);
    free_items().append(obj);
}

obj_ptr oop_space::alloc (std::size_t bytes, bool is_bitarray)
{
    obj_ptr smallest;
    constexpr auto sizeof_handle = sizeof(oop_handle);
    auto real_bytes = bytes + 
        ( (bytes % sizeof_handle)
            ? (sizeof_handle - (bytes % sizeof_handle))
            : 0 );
    auto total_size = real_bytes + sizeof(oop_obj);
    obj_ptr cur = free_items().get_head();

    while(cur)
    {
        //std::cout << cur.get_offset() << std::endl;


        auto cursize = cur->get_size();
        if(cursize == real_bytes)
        {
            free_items().remove(cur);
            new(cur.get()) oop_obj(bytes, is_bitarray);
            used_items().prepend(cur);
            return cur;
        }
        if(cursize > real_bytes &&
                (!smallest || cursize < smallest->get_size()))
            smallest = cur;//.get();
        cur = cur->get_next();
    }

    if(!smallest)
        return obj_ptr(nullptr);

    /*
    SHOWSRC(sizeof(oop_obj));
    SHOWSRC(bytes);
    SHOWSRC(smallest->get_size());
    SHOWSRC(total_size);*/

    using oop_header_t = oop_obj::oop_header_t;
    auto flags = oop_header_t::flag(is_bitarray);

    if(smallest->get_size() > total_size)
    {
        //auto end = (char*)smallest.get() + smallest->total_size();
        //auto start = end - total_size;
        free_items().remove(smallest);
        auto old_size = smallest->get_size();
        auto new_size = old_size - total_size;
        auto new_obj_p = new(smallest.get()) oop_obj(bytes, is_bitarray);
        smallest.set_offset((obj_ptr::offset_t)(smallest) + total_size);
        new(smallest.get()) oop_obj(new_size, flags|oop_header_t::flag::freed);
        //smallest->set_free();

        obj_ptr new_obj(new_obj_p);
        used_items().append(new_obj);
        free_items().append(smallest);
        return new_obj;
    }
    free_items().remove(smallest);
    new(smallest.get()) oop_obj(bytes, flags);//is_bitarray);
    used_items().append(smallest);
    return smallest;
}



const oop_handle& oop_space::space_header::globals()
{
    return get<image::globals>();
}

inline const oop_handle& oop_space::space_header::nil_class()
{
    return get<image::nil_class>();
}
inline const oop_handle& oop_space::space_header::true_class()
{
    return get<image::true_class>();

}
inline const oop_handle& oop_space::space_header::false_class()
{
    return get<image::false_class>();

}
inline const oop_handle& oop_space::space_header::int_class()
{
    return get<image::int_class>();
}
inline const oop_handle& oop_space::space_header::sym_class()
{
    return get<image::sym_class>();
}


void oop_space::setup_instance(oop_space* space)
{
    assert(oop_obj::region::base == 0);
    assert(oop_space::get_instance() == nullptr);

    oop_obj::region::base = (uintptr_t)space->data0();
    oop_space::instance = space;
}

oop_space* oop_space::create(uint64_t size, bool init)
{
    assert(oop_obj::region::base == 0);
    assert(oop_space::get_instance() == nullptr);

    auto p = new char[size];
    assert(p != 0);
    return create(size, p, init);

}


oop_space* oop_space::create(uint64_t size, char* data, bool init)
{
    assert(oop_obj::region::base == 0);
    assert(oop_space::get_instance() == nullptr);

    oop_space* res = new oop_space;
    res->start = data;
    res->size = size;
    setup_instance(res);

    if(init)
    {
        new(res->this_header()) space_header();
        auto size_left = size - sizeof(space_header) - sizeof(oop_obj);



        auto iter = [](obj_ptr obj)->void
        {
            std::cout << obj->get_size() << std::endl;
        };

        res->free_items().iterate(iter);
        std::cout << "------------" << std::endl;
        std::cout << size_left << std::endl;

        auto first_free = new(res->data0()) oop_obj(size_left, true);
        obj_ptr ff(first_free);
        res->free_items().append(ff);

        res->free_items().iterate(iter);
    }
    return res;
}











