#pragma once

#include "absptr.hpp"

class oop_handle;
template<typename> class intrusive_refcount;

class oop_obj;
template<class T>
using my_abs_ptr = abs_ptr<T, uint64_t, region<oop_obj>>;

template<class Self>
class oop_header
{
    friend class oop_handle;
    friend class intrusive_refcount<my_abs_ptr<oop_obj>>;
    uint32_t data;
#ifdef CppRefcounting
    int32_t refcount;
protected:
    bool decref()
    {
        //std::cout << "* decref " << static_cast<Self*>(this)->repr() << std::endl;
        if(--refcount < 1)
        {
            assert(! is_free());
            static_cast<Self*>(this)->free();
            assert( is_free());
            return true;
        }
        return false;
    }
    void incref();

public:
    uint32_t get_refcount ()
    {
        return refcount;
    }

#endif

public:

    enum flag
    {
        bitarray = 1,
        freed = 2,
#ifdef EnableMarkAndSweep
        mark = 4,
        all_flags = mark|bitarray|freed,
        num_bits = 3
#else
        all_flags = bitarray|freed,
        num_bits = 2
#endif
    };

#ifdef EnableMarkAndSweep
protected:
    void mark() { data = data | flag::mark; }
    void unmark() { data = data & ~flag::mark; }
    bool is_marked() { return data & flag::mark; }

public:
#endif

    oop_header(uint32_t size, int flags): refcount(0)
    {
        data = (size << flag::num_bits) | flags;
    }
    oop_header(uint32_t size, bool is_bitarray): refcount(0)
    {
        data = (size << flag::num_bits) | (is_bitarray ? flag::bitarray : 0);
    }

    uint32_t get_size() const
    {
        return data >> flag::num_bits;
    }

    void set_size(std::size_t sz)
    {
        data = (sz << flag::num_bits) | (data & flag::all_flags);
    }

    bool is_bitarray() const
    {
        return bool(data & flag::bitarray);
    }

    void set_bitarray()
    {
        data = data | flag::bitarray;
    }

    bool is_free() const
    {
        return bool(data & flag::freed);
    }
protected:
    void set_free()
    {
        data |= flag::freed;
    }

};
