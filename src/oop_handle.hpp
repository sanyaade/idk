
#pragma once
#include <cstdint>
#include <string>
#include "absptr.hpp"
#include "tagged.hpp"
#include "obj_header.hpp"
#include "oop_tags.hpp"

class oop_class;
class oop_obj;



namespace unstd
{
// template<typename T>
// struct tag_conversion<my_abs_ptr<T>>
// {
//     using type = my_abs_ptr<T>;

//     static type from (const typename type::offset_t& some)
//     {
//         type my(some);
//         return my;
//     }
// };
}


using obj_ptr = my_abs_ptr<oop_obj>;

// template<class T>
// using my_tag_t = tagged<3, uint64_t,
//       // INT_TYPE, //inline int
//       // obj_ptr, //ptr to obj
//       // void, //nil
//       // void, //unused1
//       // void, //true
//       // void, //unused2
//       // void, //false
//       // void //unused3
//     void, //nil 000
//     INT_TYPE, //int 001
//     void, //unused 010
//     obj_ptr, //obj 011
//     void, // 100
//     void, // 101
//     void, //false 110
//     void //true 111
// >;
using obj_tagged = tagged<3, uint64_t,
    void, //nil 000
    INT_TYPE, //int 001
    void, // 010 - unused
    obj_ptr, //obj 011
    void, // 100 - unused
    void, // 101 - unused
    void, //false 110
    void //true 111
>;


using namespace unstd;


class oop_handle : public obj_tagged // my_tag_t<oop_obj>
{
public:
    using tag_t = obj_tagged; // my_tag_t<oop_obj>;
    using tags = ::tags;

    const oop_handle& get_class() const;

    /*
    oop_handle(): tag_t(tag_t::mk<TAG_NIL>())
    {
    }*/

    inline ~oop_handle();

    inline explicit oop_handle(const obj_ptr& ptr);
    inline explicit oop_handle(const INT_TYPE& num): tag_t(num)
    {
    }

    inline oop_handle& operator= (const oop_handle& other);
    //inline oop_handle& operator= (oop_handle&& other);

    /*
    static inline oop_handle int_obj(int32_t num)
    {
      return oop_handle::mk((INT_TYPE)num);
    }*/

    inline explicit oop_handle(const tag_t& h): tag_t(h)
    {
    }
    inline oop_handle(): tag_t(TAG_NIL, tag_detail())
    {
    }

    inline oop_handle(const oop_handle& other);


private:
    /*
    template<typename T>
    explicit oop_handle (const T& other): tag_t(other)
    {
    }
    */
    inline explicit oop_handle(std::size_t tag, tag_t::tag_detail td): tag_t(tag,td)
    {
    }

public:
    template<typename T>
    static inline oop_handle mk (const T& arg)
    {
        return oop_handle(arg);
    }
    template<std::size_t idx>
    static inline oop_handle mk ()
    {
        static_assert(std::is_void<tag_t::ty<idx>>::value, "type must be void");
        return oop_handle(idx, tag_t::tag_detail());
    }

    //using tag_t::set;


    static const oop_handle true_obj;
    static const oop_handle false_obj;
    static const oop_handle nil_obj;

    static inline oop_handle boolean(bool b)
    {
        return b ? true_obj : false_obj;
    }

    inline bool is_nil() const
    {
        return get_tag() == tags::Nil;
    }
    inline bool is_int() const
    {
        return get_tag() == tags::Int;
    }
    inline bool is_obj() const
    {
        return get_tag() == tags::Obj;
    }
    inline bool is_falsy() const
    {
        return (get_tag() & 1) == 0;
    }
    inline bool is_truthy() const
    {
        return (bool)(get_tag() & 1);
    }


    std::string repr () const;

    inline INT_TYPE get_int() const
    {
        if(get_tag() == TAG_INT) return raw_get<TAG_INT>();
        return 0;
    }
    inline INT_TYPE rawget_int() const
    {
        return raw_get<TAG_INT>();
    }

    inline obj_ptr get_obj() const
    {
        if(is_obj()) return get<TAG_OBJ>();
        return obj_ptr();
    }
    inline obj_ptr rawget_obj() const
    {
        return raw_get<TAG_OBJ>();
    }

};
