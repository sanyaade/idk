
#pragma once

// 00 int
// 01 ptr
// 10 nil
// 11 (unused)
//100 true
//101 (unused)
//110 false
//111 (unused)

#define TAG_OBJ tags::Obj
#define TAG_INT tags::Int
#define TAG_NIL tags::Nil
#define TAG_FALSE tags::False
#define TAG_TRUE  tags::True

#define INT_TYPE   int64_t

enum tags
{
    // Int = 0b000,
    // Obj = 0b001,
    // Nil = 0b010,
    // False=0b110,
    // True= 0b100,
    // Falsy=0b010

    Nil = 0b000,
    Int = 0b001,
    Obj = 0b011,
    True= 0b111,
    False=0b110

    //falsy = tag & 1 == 0
    //truthy = tag & 1
};

// 000 nil
// 001 number
// 011 object
// 111 true
// 110 false
//
// xx0 falsy
// xx1 truthy
