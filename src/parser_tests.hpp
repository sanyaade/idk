#pragma once
#include <string>
#include <vector>
#include "codegen.hpp"
#include "grammar.hpp"

namespace tests
{

struct test_set
{

    template<class A, class B>
    static bool test (const A& parser, const std::string& str, B& result)
    {
        std::string::const_iterator start = str.begin(), end = str.end();
        if(phrase_parse(start, end, parser, ascii::space, result))
            return true;
        return false;
    }

    template<class Parser, class Result, class Visitor>
    static bool test (const Parser& parser, const std::string& str, Result& result, const Visitor& visitor)
    {
        if(test(parser, str, result))
        {
            boost::apply_visitor(visitor, result);
            std::cout << std::endl;
            return true;
        }
        return false;
    }





    std::string name;
    std::vector<std::string> tests;

    test_set (const std::string& name, const std::initializer_list<std::string> &tests):
        name(name), tests(tests)
    {
    }

    void report_failure (const std::string& s) const
    {
        std::cout << "  " << s << " failed to parse" << std::endl;
    }

    template<typename Parser>
    void run (const Parser& p) const
    {
        int correct = 0;
        for(const auto& s: tests)
        {
            ast::expr result;
            if(test_set::test(p, s, result, ast_print()))
                correct++;
            else
                report_failure(s);
        }
        std::cout << "[" << name << "] " << correct << "/" << tests.size() << " correct." << std::endl;
    }
};


template<
    typename Grammar = g2<std::string::const_iterator>
>
void run_parser_tests ()
{
    Grammar grammar;

    std::vector<test_set> tests{

        test_set("simple terminals", {
            "hello",
            "Const",
            "#sym",
            "#at:",
            "#at:put:",
            "#Const",
            "42",
            "nil",
            "true",
            "false",
            "self"
        }),
        test_set("strings", {
            "'hello'",
            "'\\'\\\\'"
        }),
        test_set("arrays", {
            "#()",
            "#(0)",
            "#('a'. 'b')"
        }),
        test_set("unary messages", {
            "1 x",
            "1 x y",
            "(1 x) y"
        }),
        test_set("binary messages", {
            "1 + 2",
            "1 + 2 - 3",
            "(1 + 2) - 3",
            "1 + (2 - 3)",
            "1 < 2 << 3"
        }),
        test_set("keyword messages", {
            "1 x: 2",
            "1 x: 2 y: 3",
            "self at: 1 put: 2"
        }),
        test_set("message sends", {
            "1 + 2 x: 3",
            "1 + (2 x: 3)"
        }),
        test_set("assignment", {
            "x := 1",
            "x := 1 at: 2"
        }),
        test_set("blocks", {
            "[1+2]",
            "[|:in1| in1 * 2]",
            "[|local| nil]",
            "[|:in1 loc1| in1 + loc1 ]",
            "[|:x| [ x < 10 ] whileTrue: [ x := x + 1 ]]",
            "[ <0 self 1>. nil. ] "
        })

    };

    for(const auto& it: tests)
        it.run(grammar);
}

}
