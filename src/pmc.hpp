

struct PMC
{
private:
    struct item
    {
        uintptr_t key;
        obj_ptr klass, method;
        bool is_method_missing;
    };
    std::vector<item> cache;

    bool fallback_lookup(obj_ptr klass, obj_ptr selector, obj_ptr& out)
    {
        for(obj_ptr k = klass; !k.is_null(); k = k->get_field(oop_class_slots::superclass).get_obj())
        {
            const oop_handle& methods = k->get_field(oop_class_slots::methods);
            if(methods.is_nil())
                continue;
            const oop_handle& m = dict_search(methods, selector->cstr());
            if(m.is_nil())
                continue;
            out = m.get_obj();
            return false;
        }
        for(obj_ptr k = klass; !k.is_null(); k = k->get_field(oop_class_slots::superclass).get_obj())
        {
            const oop_handle& methods = k->get_field(oop_class_slots::methods);
            if(methods.is_nil())
                continue;
            const oop_handle& m = dict_search(methods, MISSING_MESSAGE_NAME);
            if(m.is_nil())
                continue;
            out = m.get_obj();
            return true;
        }
        //shouldn't get here
        assert(false);
    }


public:
    PMC()
    {
        cache.reserve(1024);
    }
    void flush()
    {
        cache.clear();
    }

    bool lookup (obj_ptr klass, obj_ptr selector, obj_ptr& method)
    {
        uintptr_t key = klass.get_offset() ^ selector.get_offset();
        
        auto ptr = std::lower_bound(cache.begin(), cache.end(), key,
            [](const item& a, uintptr_t b)
            {
                return a.key < b;
            }
        );
        for(; ptr != cache.end() && ptr->key == key; ++ptr)
            if(ptr->klass == klass)
            {
            	method = ptr->method;
            	return ptr->is_method_missing;
            }
        
        //normal lookup
        bool res = fallback_lookup(klass, selector, method);
        cache.insert(ptr, item{ key, klass, method, res });
        return res;
    }
};
