#pragma once

namespace vm
{

enum primitive
{
    obj_inslot_put,
    obj_inslot,
    obj_eqeq,
    obj_send_args = 8,
    obj_signal,

    int_add = 10,
    int_sub,
    int_mul,
    int_div,
    int_mod,
    int_and,
    int_or,
    int_lt, //17
    int_shl,
    int_shr, //19

    obj_alloc = 20,
    byt_alloc,
    byt_byteat,
    byt_byteat_put,
    byt_replace, //byt_copyfrom,

    obj_size,
    byt_size,

    block_activate = 30,
    process_tick = 34,

    str_print = 99

};

std::string primitive_to_str(primitive p)
{
	switch(p)
	{
    case obj_inslot_put: return "obj_inslot_put";
    case obj_inslot: return "obj_inslot";
    case obj_eqeq: return "obj_eqeq";
    case obj_send_args: return "obj_send_args";
    case obj_signal: return "obj_signal";

	case int_add: return "int_add";
	case int_sub: return "int_sub";
	case int_mul: return "int_mul";
	case int_div: return "int_div";
	case int_mod: return "int_mod";
	case int_and: return "int_and";
	case int_or: return "int_or";
	case int_lt: return "int_lt";
	case int_shl: return "int_shl";
	case int_shr: return "int_shr";

    case obj_alloc : return "obj_alloc";
    case byt_alloc: return "byt_alloc";
    case byt_byteat: return "byt_byteat";
    case byt_byteat_put: return "byt_byteat_put";
    case byt_replace: return "byt_replace";// case byt_copyfrom: return "byt_copyfrom";

    case obj_size: return "obj_size";
    case byt_size: return "byt_size";
    case block_activate: return "block_activate";
    case process_tick: return "process_tick";

    case str_print: return "str_print";

	default: return std::to_string((int)p);
	}
}

}
