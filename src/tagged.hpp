#pragma once
#include <cassert>
#include <type_traits>


namespace unstd
{





// index<idx, TS...> returns the type at index idx
template<std::size_t idx, typename... txs>
struct index;

template<std::size_t idx, typename tx, typename... txs>
struct index<idx, tx, txs...>
{
    using type = typename index<idx-1, txs...>::type;
};
template<typename tx, typename... txs>
struct index<0, tx, txs...>
{
    using type = tx;
};





// find_index<T, TS...> finds the index of T in TS

template<typename...>
struct find_index;

template<typename t, typename... rest>
struct find_index<t, t, rest...>:
        std::integral_constant<std::size_t, 0>
{
};
template<typename t, typename next, typename... rest>
struct find_index<t, next, rest...>:
        std::integral_constant<std::size_t, 1 + find_index<t,rest...>::value>
{
};




// void-safe sizeof()

template<typename t>
constexpr std::size_t safesize()
{
    return sizeof(t);
}

template<>
constexpr std::size_t safesize<void>()
{
    return 0;
}


static_assert(safesize<void>() == 0, "safesize<void>() == 0");
static_assert(safesize<char>() == sizeof(char), "safesize<char>() == sizeof(char)");



// valid_size<T, TS..> validates that all TS... are of equal or lesser size as T

template<typename, typename...>
struct valid_size;

template<typename max, typename t>
struct valid_size<max,t>
{
    constexpr static bool value = safesize<t>() <= sizeof(max);
};

template<typename max, typename t, typename... ts>
struct valid_size<max,t,ts...>
{
    constexpr static bool value = safesize<t>() <= sizeof(max) && valid_size<max,ts...>::value;
};


// first_void<ts...>::index index starting at 0 to find the first void type
template<std::size_t, typename...>
struct first_void;

template<std::size_t idx, typename T, typename... TS>
struct first_void<idx,T,TS...>
{
    constexpr static std::size_t value = std::is_void<T>::value ? idx : first_void<idx+1, TS...>::value;
};
template<std::size_t idx, typename T>
struct first_void<idx,T>
{
    constexpr static std::size_t value = std::is_void<T>::value ? idx : -1;
};



// template<typename...>
// struct tag_conversion;

// template<typename T>
// struct tag_conversion<T>
// {
//     using type = T;

//     template<typename U>
//     static T from (const U& some)
//     {
//         T my(some);
//         return my;
//         //return (T)(some);
//     }
// };

/*
template<typename... T>
struct tag_conversion<abs_ptr<T...>>
{
  using type = abs_ptr<T...>;

  static abs_ptr<T...> from (const typename type::offset_t& some)
  {
    abs_ptr<T...> my(some);
    return my;
  }
};
*/

}


using namespace unstd;

template<
std::size_t TagBits,
    typename Bits,
    typename... Tys
    >
class tagged
{
protected:
    static constexpr Bits tag_mask = ((Bits)1 << TagBits) - 1;
    static constexpr Bits rel_mask = ~tag_mask;

    Bits bits;

    static_assert(valid_size<Bits, Tys...>::value, "not all types are <= sizeof(bits)?");

    explicit tagged (const Bits& bits): bits(bits)
    {
    }

public:



    template<std::size_t tag>
    using ty = typename index<tag, Tys...>::type;

    template<typename T>
    constexpr static std::size_t find = find_index<T, Tys...>::value;

    using self_t = tagged<TagBits, Bits, Tys...>;
    using tag_t  = self_t;
    using bits_t = Bits;


    inline tagged (const self_t& other): bits(other.bits)
    {
    }

    struct tag_detail {};
    inline tagged (std::size_t tag, const tag_detail& unused): bits((Bits)tag)
    {
    }

    /*
    template<std::size_t idx>
    explicit inline tagged (const ty<idx>& other)
    {
      set<idx>(other);
    }*/
    template<typename x>
    explicit inline tagged (const x& other)
    {
        set<find<x>>(other);
    }

    template<std::size_t tag>
    void set(ty<tag> val)
    {
        static_assert((tag & rel_mask) == 0, "invalid tag");
        bits = static_cast<Bits>(val << TagBits) | tag;
    }


    template<std::size_t tag>
    static inline self_t mk (const ty<tag>& value)
    {
        //self_t my(tag, tag_detail());
        //my.set<tag>(value);

        // self_t my(value);
        // return my;
        return self_t(value);

    }

    template<std::size_t tag>
    static inline self_t mk ()
    {
        static_assert((tag & rel_mask) == 0, "invalid tag");
        static_assert(std::is_void<ty<tag>>::value, "non-void tag type used with no value");
        self_t my(tag,tag_detail());
        return my;
    }

    inline Bits get_tag() const
    {
        return bits & tag_mask;
    }

    template<std::size_t tag>
    inline ty<tag> get() const
    {
        //static_assert(std::is_pod<ty<tag>>::value, "type isnt POD");
        static_assert((tag & rel_mask) == 0, "invalid tag");
        assert(tag == get_tag());
        return raw_get<tag>();
    }

    template<std::size_t tag>
    inline ty<tag> raw_get() const
    {
        return (ty<tag>)((bits & rel_mask) >> TagBits);
    }

    template<class U>
    static inline self_t copy (const U& other)
    {
        self_t my(other.bits);
        return my;
    }

    inline void reset(std::size_t tag)
    {
        bits = (Bits)tag & tag_mask;
    }

    inline bool operator==(const tagged& other) const
    {
        return bits == other.bits;
    }
    inline bool operator!=(const tagged& other) const
    {
        return !(*this == other);
    }

};

