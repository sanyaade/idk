class z;
struct z_region;
//using z_region = region<z>;
using zptr = abs_ptr<z, uint32_t, z_region>;

class z: public linked_item<zptr>
{
public:
    int s;

    static int instances;
    z(): s(z::instances++)
    {
    }

    bool operator== (const z& other) const
    {
        return s == other.s;
    }
};
int z::instances(0);

struct z_region
{
    static const uintptr_t base = 0;
};
//template<> uintptr_t z_region::base(0);



void test1()
{
    //testx();
    z::instances = 0;
    z* z_arr = new z[10];
    //z_region::base = (uintptr_t)(z_arr);
    z::ptr_t zs[10];

    linked_list<z::ptr_t> ll;

    for(int i = 0; i < 10; ++i)
    {
        zs[i] = &z_arr[i];
        //std::cout << (i == z_arr[i].s) << std::endl;
        //std::cout << zs[i].get_offset() << std::endl;
        z::ptr_t x(&z_arr[i]);
        //std::cout << ' ' << x.get_offset() << std::endl <<
        //  "  " << (uintptr_t)x.get() << std::endl;
        ll.append(x);
    }

    auto iter = [](z::ptr_t item)->void
    {
        std::cout << item->s << ' ';
    };
    ll.iterate(iter);
    std::cout << std::endl;
    ll.remove(zs[3]);
    ll.iterate(iter);
    std::cout << std::endl;
    ll.iterate2(iter);
    std::cout << std::endl;
}






template<class T>
class ptr
{
    T* p;

public:
    //ptr(std::nullptr_t): p(0) {}
    ptr(T* p): p(p) {}
    ptr(): p(0) {}

    inline operator bool() const
    {
        return p != nullptr;
    }
    inline T* operator->() const
    {
        return p;
    }

    inline bool operator==(const ptr<T>& other)
    {
        return p == other.p;
    }
};



void test2()
{
    class foo: public linked_item<ptr<foo>>
    {
    public:
        foo(int x): x(x), linked_item() {}

        int x;
    };
    linked_list<ptr<foo>> ll;
    ptr<foo> f1 = new foo(1);
    ptr<foo> f2 = new foo(2);
    ll.append(f1);
    ll.append(f2);

    auto iter_f = [](ptr<foo> it)->void
    {
        std::cout << it->x << ' ' ;
    };

    ll.iterate(iter_f);
    std::cout << std::endl;
    ll.iterate2(iter_f);
    std::cout << std::endl << "---------" << std::endl <<
              "removing " << f1->x << std::endl <<
              "tail = " << ll.get_tail()->x << std::endl;


    ll.remove(f1);
    ll.iterate(iter_f);
    std::cout << std::endl;
    ll.iterate2(iter_f);
    std::cout << "---------" << std::endl;

    ptr<foo> pf1 = f1;
    ptr<foo> pf12 = f1;

    std::cout << "---------" << std::endl
              << (pf1 == pf12) << std::endl;


    /*
    auto it = ll.getHead();
    while(it)
    {
      std::cout << it->x << ' ';
      it = it->getNext();
    }
    std::cout << std::endl;

    ll.remove(f2);
    */

}










