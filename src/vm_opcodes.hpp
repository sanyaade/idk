#pragma once

namespace vm {
  enum opcode : unsigned char
  {
    halt = 1, pop, ret, method_end, block_end, //5
    send, send_self, send_super, //8
    push_i32, push_nil, push_self, push_context, push_block, //13
    push_true, push_false, push_string, //16
    call_primitive, load_symbol, load_const, load_immediate, //20
    get_upvalue, set_upvalue, get_slot, set_slot, //24
    push_array, //25
    jump, jump_ift, jump_iff, peek_jump_ift, peek_jump_iff, //30

    add, sub, mul, div, mod, //35
    eq, neq, lt, gt, lteq, gteq, //41
    shl, shr,



    illegal
  };

  std::string opcode_to_str (opcode op)
  {
    switch(op)
    {
      case halt: return "halt";
      case pop: return "pop";
      case ret: return "ret";
      case method_end: return "method_end";
      case block_end: return "block_end";
      case send: return "send";
      case send_self: return "send_self";
      case send_super: return "send_super";
      case push_i32: return "push_i32";
      case push_nil: return "push_nil";
      case push_self: return "push_self";
      case push_context: return "push_context";
      case push_block: return "push_block";
      case push_true: return "push_true";
      case push_false: return "push_false";
      case push_string: return "push_string";
      case call_primitive: return "call_primitive";
      case load_symbol: return "load_symbol";
      case load_const: return "load_const";
      case load_immediate: return "load_immediate";
      case get_slot: return "get_slot";
      case get_upvalue: return "get_upvalue";
      case set_slot: return "set_slot";
      case set_upvalue: return "set_upvalue";
      case push_array: return "push_array";
      //case retry_method: return "retry_method";
      case jump: return "jump";
      case jump_ift: return "jump_ift";
      case jump_iff: return "jump_iff";
      case peek_jump_ift: return "peek_jump_ift";
      case peek_jump_iff: return "peek_jump_iff";
      case add: return "add";
      case sub: return "sub";
      case mul: return "mul";
      case div: return "div";
      case mod: return "mod";
      case eq:  return "eq";
      case neq: return "neq";
      case lt:  return "lt";
      case gt:  return "gt";
      case lteq: return "lteq";
      case gteq: return "gteq";

      default: return std::to_string((int)op);
    }
  }

  namespace bytecode {
    struct block_header
    {
      int16_t len, locals;
    };
    struct upvalue
    {
      uint16_t parents, index;
    };
  } //bytecode
} //vm
